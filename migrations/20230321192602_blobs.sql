-- Add migration script here
create table if not exists blobs (
    id          text     not null primary key ,
    uploaded_by integer  not null ,
    uploaded_at datetime not null default current_timestamp,
    data        blob     not null 
)