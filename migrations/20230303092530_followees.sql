-- Add migration script here
create table if not exists followees (
    follower_id    integer      not null ,
    user_id        text         not null ,

    followed_at    datetime     not null default current_timestamp , -- UTC
    updated_at     datetime     ,

    foreign key (follower_id) references users(id),
    foreign key (user_id) references external_users(id)
    primary key (user_id, follower_id)
);