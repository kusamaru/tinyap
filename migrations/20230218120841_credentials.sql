-- Add migration script here
create table if not exists credentials (
    user_id    integer  not null primary key,
    mail       text     not null ,
    passhash   text     not null ,
    updated_at datetime not null default current_timestamp ,

    foreign key (user_id) references users(id)
);