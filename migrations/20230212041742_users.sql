-- Add migration script here
create table if not exists users (
    id                 integer      not null primary key autoincrement ,
    preferred_username varchar(64)  not null unique ,
    name               varchar(64)  not null ,
    summary            varchar(240) ,
    timezone           integer      ,
    icon_blob          text         ,
    joined_at          datetime     not null default current_timestamp ,
    updated_at         datetime     ,

    check (-13 < timezone AND timezone < 15)
    foreign key (icon_blob) references blobs(id)
);