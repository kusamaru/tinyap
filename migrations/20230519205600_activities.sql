-- Add migration script here
create table activities (
    object_id   text     not null primary key ,
    object_json text     not null , -- use as json...
    created_at  datetime not null default current_timestamp,
    updated_at  datetime ,
    deleted_at  datetime 
)