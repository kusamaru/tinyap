-- Add migration script here
create table if not exists keys (
    user_id    integer  not null primary key,
    pubkey     text     not null,
    secret     text     not null,
    created_at datetime not null default current_timestamp ,

    foreign key (user_id) references users(id)
);