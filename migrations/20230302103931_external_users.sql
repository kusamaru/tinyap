-- Add migration script here
create table if not exists external_users (
    id             text     not null primary key,
    name           text     ,
    inbox          text     ,
    shared_inbox   text     ,
    publickey      text     ,
    registered_at  datetime not null default current_timestamp , -- UTC
    updated_at     datetime
);