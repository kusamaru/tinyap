-- Add migration script here
create table activities_received_users (
    object_id        text     not null ,
    received_user_id integer  not null ,

    received_at      datetime not null default current_timestamp ,

    foreign key (object_id) references activities(object_id),
    foreign key (received_user_id) references users(id)
)