-- Add migration script here
create table attachments (
    object_id text not null ,
    blob_id text not null ,

    foreign key (object_id) references activities(object_id)
    foreign key (blob_id) references blobs(id)
)