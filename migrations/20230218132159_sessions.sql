-- Add migration script here
create table if not exists sessions (
    session_id text     not null primary key ,
    user_id    integer  not null ,
    user_agent text     ,
    created_at datetime not null default current_timestamp, -- UTC
    expires_at datetime not null ,                          -- UTC

    foreign key (user_id) references users(id)
);