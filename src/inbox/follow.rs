use http::StatusCode;

use crate::{response, db, get_actor_data, get_name_from_url};

pub async fn follow(req: serde_json::Value) -> Result<(), StatusCode> {
    let bad_req = StatusCode::BAD_REQUEST;

    log::info!("follow request accept");

    let Some(object) = req.get("object") else { return Err(bad_req) };
    // get follower data
    let Some(actor) = req.get("actor") else { return Err(bad_req) };
    let Ok(data) = get_actor_data(&actor.as_str().ok_or(bad_req)?.to_string()).await else { return Err(bad_req) };

    // get "/host/u/{name}"
    let user_url = object.as_str().ok_or(bad_req)?;
    let pref_name = get_name_from_url(&user_url).ok_or(bad_req)?;

    let follower_id = data.get("id")
        .and_then(|val| val.as_str())
        .ok_or(bad_req)?;
    let follower_inbox = data.get("inbox")
        .and_then(|val| val.as_str())
        .map(|s| s.to_string());
    let follower_name = data.get("name")
        .and_then(|val| val.as_str())
        .map(|s| s.to_string());
    let shared_inbox = data.get("sharedInbox")
        .and_then(|val| val.as_str())
        .map(|s| s.to_string());

    log::info!("sending accept...");
    response::accept(follower_inbox.clone().ok_or(bad_req)?, pref_name.clone(), &req)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    log::info!("accept response success");

    match db::insert_follower(
        pref_name,
        follower_id.to_string(),
        follower_name,
        follower_inbox,
        shared_inbox,
    ).await {
        Ok(_) => {
            log::info!("DB insert success");
            Ok(())
        },
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR)
    }
}

pub async fn accepted_follow(req: &serde_json::Value) -> Result<(), StatusCode> {
    let data = req.get("object").ok_or(StatusCode::BAD_REQUEST)?;

    let pref_name = data.get("object")
        .and_then(|val| val.as_str())
        .map(|s| s.to_string())
        .ok_or(StatusCode::BAD_REQUEST)?;
    let followee_user_id = data.get("id")
        .and_then(|val| val.as_str())
        .map(|s| s.to_string())
        .ok_or(StatusCode::BAD_REQUEST)?;
    let inbox = data.get("inbox")
        .and_then(|val| val.as_str())
        .map(|s| s.to_string());
    let name = data.get("name")
        .and_then(|val| val.as_str())
        .map(|s| s.to_string());
    let shared_inbox = data.get("sharedInbox")
        .and_then(|val| val.as_str())
        .map(|s| s.to_string());
    match db::insert_followee(pref_name, followee_user_id, name, inbox, shared_inbox).await {
        Ok(_) => {
            log::info!("follow accept");
            Ok(())
        },
        Err(_) => {
            Err(StatusCode::UNAUTHORIZED)
        }
    }
}

pub async fn unfollow(req: &serde_json::Value) -> Result<(), StatusCode> {
    let bad_req = StatusCode::BAD_REQUEST;

    let Some(object) = req.get("object") else { return Err(bad_req) };
    let Some(actor) = req.get("actor") else { return Err(bad_req) };
    let Ok(data) = get_actor_data(&actor.as_str().ok_or(bad_req)?.to_string()).await else { return Err(bad_req) };

    let follower_id = data.get("id")
        .and_then(|val| val.as_str())
        .ok_or(bad_req)?;
    let follow_target_user = object.get("object")
        .and_then(|val| val.as_str())
        .ok_or(bad_req)?;
    let pref_name = get_name_from_url(&follow_target_user).ok_or(bad_req)?;

    // match db::remove_follower(pref_name, follower_id.into()).await {
    //     Ok(_) => Ok(()),
    //     Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR)
    // }
    let _ = db::remove_follower(pref_name, follower_id.into()).await;
    Ok(())
}