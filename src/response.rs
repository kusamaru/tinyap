use std::collections::HashSet;

use serde::Serialize;

use crate::{db, structs::{Activity, AnyActivity}, auth};

async fn get_key<'a>(pref_name: &String) -> anyhow::Result<Vec<u8>> {
    let Some((_, key)) = db::get_user_by_pref_name(pref_name).await? else { anyhow::bail!("user not found") };
    Ok(key.secret.as_bytes().to_vec())
}

pub async fn accept(inbox: String, pref_name: String, object: &serde_json::Value) -> anyhow::Result<()> {
    // log::info!("inbox: {}, pref_name: {}, obj: {:?}", inbox, pref_name, object);

    let json = AnyActivity::new(
        "Accept".into(),
        pref_name.to_string(),
        object
    );

    let mut req = http::Request::post(&inbox).body(json.to_string())?;
    auth::sign_header(&mut req, &inbox, &pref_name, &get_key(&pref_name).await?).unwrap();
    // log::info!("accept: request={:?}", req);

    // post
    let result = reqwest::Client::new()
        .execute(req.try_into().unwrap())
        .await?;
    if result.status().as_u16() >= 400 && result.status().as_u16() < 600 {
        log::warn!("accept fail!");
        log::warn!("{:?}", result.text().await);
        anyhow::bail!("fail");
    }
    log::info!("accepted request: object={:?}, result={:?}", object, result.text().await);
    Ok(())
}

pub async fn create<A: Activity + Serialize>(
    inboxes: Vec<String>, 
    pref_name: &String, 
    object: A, 
) -> anyhow::Result<()> {
    let client = reqwest::Client::new();
    let privkey = get_key(pref_name).await?;

    let json = AnyActivity::new(
        "Create".into(),
        pref_name.to_string(),
        object
    );

    let mut sended = HashSet::new();
    for inbox in inboxes {
        if sended.contains(&inbox) { continue; }

        let mut req = http::Request::post(&inbox).body(json.to_string())?;
        auth::sign_header(&mut req, &inbox, &pref_name, &privkey)?;

        // post
        let result = client.execute(req.try_into().unwrap()).await?;
        if result.status().as_u16() >= 400 && result.status().as_u16() < 600 {
            log::warn!("send fail!");
            log::warn!("{:?}", result.text().await);
        } else {
            log::info!("{:?}", result.text().await);
        }
        sended.insert(inbox);
    }
    log::info!("create done: inbox count = {}", sended.len());
    Ok(())
}

pub async fn follow(inbox: &str, pref_name: &String, target_user_id: &String) -> anyhow::Result<()> {
    let privkey = get_key(pref_name).await?;
    let req_json = AnyActivity::new(
        "Follow".into(),
        pref_name.to_string(),
        target_user_id.to_string()
    );

    let mut req = http::Request::post(inbox).body(req_json.to_string())?;
    auth::sign_header(&mut req, inbox, pref_name, &privkey)?;

    let result = reqwest::Client::new().execute(req.try_into().unwrap()).await?;
    if result.status().as_u16() >= 400 && result.status().as_u16() < 600 {
        log::warn!("send fail!");
        log::warn!("{:?}", result.text().await);
    } else {
        log::info!("{:?}", result.text().await);
    }
    Ok(())
}