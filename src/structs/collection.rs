use serde::{Deserialize, Serialize};

use super::DbActivity;

#[derive(Deserialize, Debug)]
pub struct PagedCollectionQuery {
    pub page: Option<u32>,
    pub from: Option<String>,
    pub to: Option<String>,
}

#[derive(Serialize)]
pub struct CollectionItem {
    pub id: String,
    #[serde(rename = "type")]
    pub i_type: String,
    pub actor: String,
    pub published: chrono::DateTime<chrono::Utc>,
    pub object: serde_json::Value,
}
impl TryFrom<serde_json::Value> for CollectionItem {
    type Error = anyhow::Error;

    fn try_from(value: serde_json::Value) -> Result<Self, anyhow::Error> {
        let Some(id) = value.get("id").and_then(|val| val.as_str()) else { anyhow::bail!("id not found") };
        let Some(i_type) = value.get("type").and_then(|val| val.as_str()) else { anyhow::bail!("type not found") };
        let Some(actor) = value.get("actor").and_then(|val| val.as_str()) else { anyhow::bail!("actor not found") };
        let Some(Ok(published)) = value.get("published")
            .and_then(|val| val.as_str())
            .map(|val| chrono::DateTime::parse_from_rfc3339(val)) else { anyhow::bail!("published not found") };
        let Some(object) = value.get("object") else { anyhow::bail!("object not found") };
        Ok(Self {
            id: id.to_string(),
            i_type: i_type.to_string(),
            actor: actor.to_string(),
            published: published.with_timezone(&chrono::Utc),
            object: object.to_owned(),
        })
    }
}
impl TryFrom<DbActivity> for CollectionItem {
    type Error = anyhow::Error;

    fn try_from(value: DbActivity) -> Result<Self, Self::Error> {
        let v = serde_json::from_str::<serde_json::Value>(&value.object_json)?;
        let Some(id) = v.get("id").and_then(|val| val.as_str()) else { anyhow::bail!("id not found") };
        let Some(i_type) = v.get("type").and_then(|val| val.as_str()) else { anyhow::bail!("type not found") };
        let Some(actor) = v.get("actor").and_then(|val| val.as_str()) else { anyhow::bail!("actor not found") };
        let Some(Ok(published)) = v.get("published")
            .and_then(|val| val.as_str())
            .map(|val| chrono::DateTime::parse_from_rfc3339(val)) else { anyhow::bail!("published not found") };
        Ok(Self {
            id: id.to_string(),
            i_type: i_type.to_string(),
            actor: actor.to_string(),
            published: published.with_timezone(&chrono::Utc),
            object: v.to_owned(),
        })
    }
}

#[derive(Serialize)]
pub struct FeedItem {
    pub id: Option<String>,
    pub actor: String,
    pub published: chrono::DateTime<chrono::Utc>,
    pub object: serde_json::Value,
}