use crate::db;

use super::{PublicKey, MediaObject};

use serde::Serialize;
use uuid::Uuid;

#[derive(Debug)]
pub struct DbUser {
    pub id: i64,
    pub preferred_username: String,
    pub name: String,
    pub summary: Option<String>,
    pub timezone: Option<i64>,
    pub icon_blob: Option<String>,
    pub joined_at: chrono::NaiveDateTime,
    pub updated_at: Option<chrono::NaiveDateTime>,
}

#[derive(Clone, Debug, Serialize)]
pub struct DbActivity {
    pub object_id: String,
    /// raw json string. DONT USE THIS DIRECTLY
    pub object_json: String,
    pub created_at: chrono::NaiveDateTime,
    pub updated_at: Option<chrono::NaiveDateTime>,
    pub deleted_at: Option<chrono::NaiveDateTime>,
}
impl DbActivity {
    pub fn try_to_json(&self) -> Result<serde_json::Value, serde_json::Error> {
        serde_json::from_str(&self.object_json)
    }
}


#[derive(Debug)]
pub struct DbActivityReceivedUser {
    pub object_id: String,
    pub received_user_id: i64,
    pub received_at: chrono::NaiveDateTime,
}

#[derive(Debug)]
pub struct DbExternalUser {
    pub id: String,
    pub name: Option<String>,
    pub inbox: Option<String>,
    pub shared_inbox: Option<String>,
    pub publickey: Option<String>,
    pub registered_at: chrono::NaiveDateTime,
    pub updated_at: Option<chrono::NaiveDateTime>,
}

#[derive(Debug)]
pub struct DbFollower {
    pub user_id: i64,
    pub follower_id: String,
    pub followed_at: chrono::NaiveDateTime,
    pub updated_at: Option<chrono::NaiveDateTime>,
}
#[derive(Debug)]
pub struct DbFollowee {
    pub user_id: String,
    pub follower_id: i64,
    pub followed_at: chrono::NaiveDateTime,
    pub updated_at: Option<chrono::NaiveDateTime>,
}

#[derive(Debug)]
pub struct DbKey {
    pub user_id: i64,
    pub pubkey: String,
    pub secret: String,
    pub created_at: chrono::NaiveDateTime,
}
impl DbKey {
    pub async fn to_key(self) -> anyhow::Result<PublicKey> {
        let u = db::get_user_by_id(self.user_id).await?;
        Ok(PublicKey {
            context: String::from("https://www.w3.org/ns/activitystreams"),
            k_type: String::from("Key"),
            id: crate::to_url(format!("/key/{}/public", self.user_id)),
            owner: crate::to_url(format!("/u/{}", u.preferred_username)),
            public_key_pem: self.pubkey /* .replace('\n', "") */
        })
    }
}

#[derive(Debug)]
pub struct DbCredential {
    pub user_id: i64,
    pub mail: String,
    pub passhash: String,
    pub updated_at: chrono::NaiveDateTime,
}

#[derive(Debug)]
pub struct DbSession {
    pub session_id: String,
    pub user_id: i64,
    pub user_agent: Option<String>,
    pub created_at: chrono::NaiveDateTime,
    pub expires_at: chrono::NaiveDateTime,
}

#[derive(Debug)]
pub struct DbReply {
    pub user_id: i64,
    pub user_note_id: i64,
    pub activity_id: String,
}

#[derive(Debug)]
pub struct DbBlob {
    pub id: String,
    pub uploaded_by: i64,
    pub uploaded_at: chrono::NaiveDateTime,
    pub data: Vec<u8>
}

#[derive(Debug)]
pub struct DbAttachment {
    pub object_id: String,
    pub blob_id: String,
}
impl DbAttachment {
    pub fn to_media(&self, blob_id: &str,) -> anyhow::Result<MediaObject> {
        let uuid = Uuid::parse_str(blob_id)?;
        Ok(MediaObject {
            m_type: String::from("Document"),
            media_type: String::from("application/octet-stream"), // FIXME: use mimeType. naruhaya
            url: crate::to_url(format!("/m/{}", uuid.as_simple())),
        })
    }
}