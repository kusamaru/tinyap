use std::fmt::Display;

use chrono::{FixedOffset, Utc, TimeZone, DateTime};
use serde::{Serialize, Deserialize};

use super::{MediaObject, PublicKey, ToCc, DbReply, DbActivity};

fn make_activity_id(datetime: &DateTime<Utc>) -> String {
    crate::to_url(format!("/a/{}", datetime.timestamp_nanos()))
}

/// marker trait
pub trait Activity: Serialize {}

#[derive(Serialize)]
pub struct PersonActor {
    #[serde(rename = "@context")]
    context: String,
    #[serde(rename = "type")]
    p_type: String,
    id: String,
    name: String,
    #[serde(rename = "preferredUsername")]
    preferred_username: String,
    summary: String,
    inbox: String,
    outbox: String,
    url: String,
    icon: Option<MediaObject>,
    #[serde(rename = "publicKey")]
    public_key: PublicKey,
}
impl PersonActor {
    pub fn new(
        id: String,
        name: String,
        preferred_username: String,
        summary: String,
        inbox: String,
        outbox: String,
        url: String,
        icon_uuid: Option<uuid::Uuid>,
        public_key: PublicKey,
    ) -> Self {
        let icon = match icon_uuid {
            Some(uuid) => 
                Some(MediaObject {
                    m_type: String::from("Image"),
                    media_type: String::from("image/png"),
                    url: crate::to_url(format!("/m/{}", uuid.as_simple()))
                }),
            None => None
        };
        Self {
            context: String::from("https://www.w3.org/ns/activitystreams"),
            p_type: String::from("Person"),
            id,
            name,
            preferred_username,
            summary,
            inbox,
            outbox,
            url,
            icon,
            public_key,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NoteObject {
    #[serde(rename = "@context")]
    context: String,
    #[serde(rename = "type")]
    n_type: String,
    id: String,
    #[serde(rename = "attributedTo")]
    attributed_to: String,
    #[serde(rename = "inReplyTo")]
    in_reply_to: Option<String>,
    attachment: Option<Vec<MediaObject>>,
    content: String,
    published: chrono::DateTime<FixedOffset>,
    replies: Option<String>,
    to: Vec<String>
}
impl Activity for NoteObject {}
impl NoteObject {
    pub fn new(
        pref_name: String,
        content: String,
        timezone: Option<i64>,
        published: chrono::NaiveDateTime,
        to: Option<Vec<String>>,
        in_reply_to: Option<String>,
        attachment: Option<Vec<MediaObject>>
    ) -> Self {
        let tz = FixedOffset::east_opt((timezone.unwrap_or(0i64) * 3600) as i32).unwrap();
        let published_utc = Utc.from_utc_datetime(&published);
        let to = to.unwrap_or(vec![String::from("https://www.w3.org/ns/activitystreams#Public")]);

        Self {
            context: String::from("https://www.w3.org/ns/activitystreams"),
            n_type: String::from("Note"),
            id: make_activity_id(&published_utc),
            attributed_to: crate::to_url(format!("/u/{pref_name}")),
            in_reply_to,
            content,
            published: published_utc.with_timezone(&tz),
            to,
            replies: None,
            attachment,
        }
    }

    pub fn get_id(&self) -> String {
        self.id.clone()
    }
    pub fn get_attributed_to(&self) -> &str {
        &self.attributed_to
    }
    pub fn get_content(&self) -> &str {
        &self.content
    }
    pub fn get_published(&self) -> &DateTime<FixedOffset> {
        &self.published
    }
}
impl Display for NoteObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", serde_json::to_string(self).map_err(|_| std::fmt::Error)?)
    }
}
impl TryFrom<DbActivity> for NoteObject {
    type Error = serde_json::Error;

    fn try_from(value: DbActivity) -> Result<Self, Self::Error> {
        let v = serde_json::from_str::<serde_json::Value>(&value.object_json)?;
        serde_json::from_value::<Self>(v).into()
    }
}

#[derive(Serialize)]
pub struct AnyActivity {
    #[serde(rename = "@context")]
    context: String,
    #[serde(rename = "type")]
    a_type: String,
    id: String,
    actor: String,
    object: serde_json::Value,
    to: Vec<String>,
    cc: Vec<String>,
}
impl AnyActivity {
    pub fn new<T: Serialize>(a_type: String, pref_name: String, object: T) -> Self {
        let now = Utc::now().timestamp();
        Self {
            context: "https://www.w3.org/ns/activitystreams".into(),
            a_type,
            id: crate::to_url(format!("/u/{pref_name}/s/{now}")),
            actor: crate::to_url(format!("/u/{pref_name}")),
            object: serde_json::to_value(object).unwrap(),
            to: vec![
                "https://www.w3.org/ns/activitystreams#Public".into(),
            ],
            cc: vec![]
        }
    }

    pub fn with_to_cc<T: Serialize>(a_type: String, pref_name: String, object: T, to_cc: ToCc<String>) -> Self {
        let now = Utc::now().timestamp();
        Self {
            context: "https://www.w3.org/ns/activitystreams".into(),
            a_type,
            id: crate::to_url(format!("/u/{pref_name}/s/{now}")),
            actor: crate::to_url(format!("/u/{pref_name}")),
            object: serde_json::to_value(object).unwrap(),
            to: to_cc.to,
            cc: to_cc.cc,
        }
    }
}
impl std::fmt::Display for AnyActivity {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", serde_json::to_string(self).unwrap())
    }
}

pub struct AnnounceActivity {

}