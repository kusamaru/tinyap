mod activity;
mod collection;
mod db;

pub use activity::*;
pub use collection::*;
pub use db::*;

use serde::{Serialize, Deserialize};

// activity vocabulary

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MediaObject {
    #[serde(rename = "type")]
    pub m_type: String,
    #[serde(rename = "mediaType")]
    pub media_type: String,
    /// URL to media
    pub url: String
}
#[derive(Serialize)]
pub struct PublicKey {
    #[serde(rename = "@context")]
    pub context: String,
    #[serde(rename = "type")]
    pub k_type: String,
    pub id: String,
    pub owner: String,
    #[serde(rename = "publicKeyPem")]
    pub public_key_pem: String,
}


// helper enum/structs
pub struct ToCc<T: AsRef<str>> {
    to: Vec<T>,
    cc: Vec<T>,
}
