use poem::{Route, get, post, EndpointExt, middleware::CookieJarManager};
use crate::endpoints;

#[poem::handler]
fn unimplemented() -> String {
    String::from("unimplemented endpoints")
}

pub fn route() -> Route {
    let r = Route::new()
        // web interface
        .at("/", get(endpoints::web::feeds))
        .at("/search", get(endpoints::web::search))

        // raplace api
        .at("/api/login", post(endpoints::raplace::login).with(CookieJarManager::new()))
        .at("/api/getfeed", get(endpoints::raplace::get_feed).with(CookieJarManager::new()))
        .at("/api/upload_media", post(endpoints::raplace::upload_media).with(CookieJarManager::new()))

        // user
        .at("/u/:pref_name", get(endpoints::ap::user))
        .at("/u/:pref_name/followers", get(unimplemented))
        .at("/u/:pref_name/following", get(unimplemented))
        .at("/u/:pref_name/inbox", get(endpoints::ap::get_inbox).post(endpoints::ap::post_inbox))
        .at("/u/:pref_name/outbox", get(endpoints::ap::get_outbox).post(endpoints::ap::post_outbox).with(CookieJarManager::new()))
        .at("/u/:pref_name/icon", get(endpoints::ap::icon))

        // data
        .at("/a/:id", get(endpoints::ap::activity))
        .at("/u/:pref_name/note/:id", get(endpoints::ap::note))
        .at("/m/:uuid", get(endpoints::ap::media))

        // well-known
        .at("/.well-known/host-meta", get(endpoints::wellknown::host_meta))
        .at("/.well-known/webfinger", get(endpoints::wellknown::webfinger))
        .at("/.well-known/nodeinfo", get(endpoints::wellknown::nodeinfo))
        .at("/nodeinfo/2.1", get(endpoints::wellknown::true_nodeinfo));

    if cfg!(feature = "debug") {
        // activate debug endpointss
        r
        // .at("/api/register", post(crate::api::register))
    } else {
        r
    }
}