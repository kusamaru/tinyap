use anyhow::Context;
use argon2::{password_hash::{self, SaltString}, Algorithm, Version, Params, Argon2, PasswordHasher, PasswordHash, PasswordVerifier};
use base64::{engine::general_purpose, Engine};
use http::{Uri, header::{HOST, DATE, CONTENT_TYPE}, HeaderValue};
use log::warn;
use rand::thread_rng;
use sigh::{SigningConfig, PrivateKey, Key, alg};

use crate::{structs::DbSession, db};

/// remove expired session every minute.
pub async fn run_session_manager() -> anyhow::Result<()> {
    loop {
        crate::db::clear_expired_sessions().await?;
        tokio::time::sleep(std::time::Duration::from_secs(60)).await;
    }
}

pub fn hashing_password(password: String) -> Result<String, password_hash::Error> {
    let salt = SaltString::generate(&mut thread_rng());
    let hash = Argon2::new(
        Algorithm::Argon2id,
        Version::V0x13,
        Params::new(19000, 2, 1, None).unwrap())
        .hash_password(password.as_bytes(), &salt)?
        .to_string();
    Ok(hash)
}

pub fn verify_password(password: String, expected_hash: String) -> Result<(), password_hash::Error> {
    let expected_hash = PasswordHash::new(&expected_hash)?;
    Argon2::default().verify_password(password.as_bytes(), &expected_hash)
}

pub fn is_session_ok(session: &DbSession, uid: i64) -> bool {
    use chrono::{TimeZone, Utc};

    let now = Utc::now();
    let expired_at = Utc.from_local_datetime(&session.expires_at).unwrap();
    if now > expired_at { return false }
    if session.user_id != uid { return false }

    true
}

pub fn sign_by_admin<T: ToString>(req: &mut http::Request<T>, url: &str) -> anyhow::Result<()> {
    sign_header(req, url, "admin", &crate::get_admin_key())
}

pub fn sign_header<T: ToString>(req: &mut http::Request<T>, url: &str, pref_name: &str, key: &[u8]) -> anyhow::Result<()> {
    let sha256 = hex::decode(sha256::digest(req.body().to_string()))?;
    let host = url.parse::<Uri>()?;
    let digest = general_purpose::STANDARD.encode(sha256);

    let header = req.headers_mut();
    header.insert(HOST, HeaderValue::from_str(host.host().ok_or(anyhow::anyhow!("error!"))?)?);
    header.insert(DATE, HeaderValue::from_str(&chrono::Utc::now().format("%a, %d %b %Y %T GMT").to_string())?);
    header.insert(CONTENT_TYPE, HeaderValue::from_str("application/json")?);
    header.insert("Digest", HeaderValue::from_str(&format!("sha-256={}", digest))?);

    let privkey = PrivateKey::from_pem(&key)?;
    SigningConfig::new(
        alg::RsaSha256, 
        &privkey, 
        crate::to_url(format!("/u/{pref_name}"))
    ).sign(req)?;

    Ok(())
}

pub async fn verify_header(req: &poem::Request, json: &serde_json::Value) -> anyhow::Result<()> {
    let mut builder = http::Request::builder()
        .method(req.method())
        .uri(req.uri());
    let heads = builder.headers_mut().unwrap();
    req.headers().iter().for_each(|(k, v)| { heads.insert(k, v.clone()); });

    let req = builder.body(serde_json::to_vec(json)?).context("failed to build request")?;

    let actor_id = json.get("actor")
        .or(json.get("attributeTo"))
        .and_then(|v| v.as_str())
        .map(|s| s.to_string())
        .context("Actor data is missing")?;

    let publickey = match db::is_external_user_exists(&actor_id).await {
        Ok(true) => {
            match db::get_external_user(&actor_id)
                .await?
                .publickey
            {
                Some(pem) => sigh::PublicKey::from_pem(pem.as_bytes())?,
                None => {
                    warn!("user '{}' publickey is not found in DB", actor_id);
                    let pem = get_publickey_by_uid(&actor_id).await?;
                    db::update_publickey(&actor_id, &pem).await?;
                    sigh::PublicKey::from_pem(pem.as_bytes())?
                }
            }
        },
        Ok(false) => {
            let pem = get_publickey_by_uid(&actor_id).await?;
            sigh::PublicKey::from_pem(pem.as_bytes())?
        },
        Err(e) => {
            anyhow::bail!("database error: {}", e.to_string());
        },
    };

    match sigh::Signature::from(&req).verify(&publickey) {
        Ok(true) => Ok(()),
        Ok(false) => anyhow::bail!("verification failed: publickey not match"),
        Err(e) => anyhow::bail!("verification failed: {}", e.to_string()),
    }
}

async fn get_publickey_by_uid(actor_id: &str) -> anyhow::Result<String> {
    let data = crate::get_actor_data(actor_id).await?;
    let pem = data.get("publicKey")
        .and_then(|v| v.get("publicKeyPem"))
        .and_then(|v| v.as_str())
        .context("publicKey not found")?;
    Ok(pem.into())
}