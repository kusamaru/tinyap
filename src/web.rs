use poem::{listener::{Listener, TcpListener, RustlsConfig, RustlsCertificate}, Server};

pub async fn run(addr: String, cert: String, key: String) -> Result<(), std::io::Error> {
    let app = crate::route::route();
    #[cfg(not(feature="http"))]
    let listener = {
        log::info!("cert: {:?}, key: {:?}", cert, key);
        let cert = std::fs::read(cert)?;
        let key = std::fs::read(key)?; 
        TcpListener::bind(addr.clone())
            .rustls(RustlsConfig::new()
                .fallback(RustlsCertificate::new()
                    .key(key)
                    .cert(cert)
                )
            )
    };
    #[cfg(feature="http")]
    let listener = TcpListener::bind(addr.clone());
    
    log::info!("web: port bind success");
    log::info!("server running on {}", addr);
    Server::new(listener)
        .name("tinyap")
        .run(app)
        .await
}
