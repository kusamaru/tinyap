pub mod ap;
pub mod raplace;
pub mod web;
pub mod wellknown;

use http::{StatusCode, HeaderMap};
use poem::{Response, web::cookie::CookieJar};
use serde::Serialize;
use serde_json::json;

use crate::{db, ACT_JSON, TRUE_TYPE, auth::is_session_ok};

const SESSION_ID_COOKIE: &str = "session_id";

async fn get_uid(pref_name: &String) -> Result<i64, Response> {
    match db::get_uid(&pref_name).await {
        Ok(Some(i)) => Ok(i),
        Ok(None) => Err(resp("user not found", StatusCode::NOT_FOUND)),
        Err(_) => Err(resp("database error", StatusCode::INTERNAL_SERVER_ERROR)), 
    }
}

fn check_header(headers: &HeaderMap) -> bool {
    //  content-type check
    if  matches!(headers.get("Content-Type"), Some(val) if val == ACT_JSON || val == TRUE_TYPE) || 
        matches!(headers.get("content-type"), Some(val) if val == ACT_JSON || val == TRUE_TYPE) ||
        matches!(headers.get(http::header::CONTENT_TYPE), Some(val) if val == ACT_JSON || val == TRUE_TYPE)
    {
        return true
    }
    false
}

async fn check_session(cookies: &CookieJar) -> Result<(), Response> {
    let Some(id_cookie) = cookies.get(SESSION_ID_COOKIE) else {
        // session not found
        return Err(resp("session_id not found", StatusCode::UNAUTHORIZED))
    };
    let session_id = id_cookie.value_str();

    let Ok(crate::structs::DbUser { preferred_username, .. }) = db::get_user_by_session(session_id).await else {
        return Err(resp("bad session", StatusCode::UNAUTHORIZED))
    };

    let uid = match get_uid(&preferred_username).await {
        Ok(i) => i, Err(e) => return Err(e),
    };
    let Ok(db_session) = db::get_session(session_id).await else {
        return Err(resp("expired/bad session", StatusCode::UNAUTHORIZED))
    };
    if !is_session_ok(&db_session, uid) {
        // bad session
        return Err(StatusCode::UNAUTHORIZED.into())
    }

    Ok(())
}

/**
 * Try to infer MIME type of input byte-stream
 */
fn try_get_mime(bytes: &[u8]) -> String {
    let infer = infer::get(bytes);
    infer.map(|v| v.mime_type().to_string())
        .unwrap_or(String::from("application/octet-stream"))
}

// check mime type is media or not
fn is_mime_media(mime_type: &str) -> bool {
    let Ok(parsed) = mime_type.parse::<mime::Mime>() else { return false };
    match parsed.type_() {
        mime::AUDIO | 
        mime::VIDEO |
        mime::IMAGE => true,
        _ => false
    }
}

fn resp(body: impl Into<String>, status: StatusCode) -> Response {
    Response::builder()
        .status(status)
        .body(body.into())
}

fn to_ordered_collection_json<T: Serialize>(
    page: u32,
    _pref_name: String,
    part_of: String,
    query: Option<String>,
    body: Vec<T>,
) -> Response {
    let query = query.unwrap_or_default();
    let next = if !body.is_empty() { 
        Some(format!("{}?{}page={}", part_of, query, page+1))
    } else { None };
    let prev = if page > 1 {
        Some(format!("{}?{}page={}", part_of, query, page-1))
    } else { None };
    let _object = serde_json::to_value(body).unwrap();
    let resp = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "OrderedCollectionPage",
        "next": next,
        "prev": prev,
        "partOf": part_of,
        "orderedItems": _object,
    });
    to_activity_json(resp)
}

fn to_activity_json<T: Serialize>(body: T) -> Response {
    Response::builder()
        .content_type("application/activity+json")
        .body(serde_json::to_string_pretty(&body).unwrap())
}