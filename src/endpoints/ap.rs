//! endpoints that return any activitypub object

use http::{StatusCode, HeaderMap};
use poem::{handler, Result, web::{Path, Json, cookie::CookieJar, Query}, Response};
use uuid::Uuid;

use crate::{db, inbox, outbox, endpoints::{check_session, check_header, resp, get_uid, to_ordered_collection_json, to_activity_json}, auth::verify_header, structs::{CollectionItem, PagedCollectionQuery, PersonActor}};

#[handler]
pub async fn user(Path(pref_name): Path<String>) -> Result<Response> {
    log::info!("user access: pref_name={}", pref_name);

    let pref_name = pref_name.replace(".json", "");
    let user = crate::db::get_user_by_pref_name(&pref_name).await;
    match user {
        Ok(Some((u, k))) => {
            Ok(to_activity_json(
                PersonActor::new(
                    crate::to_url(format!("/u/{}", u.preferred_username)),
                    u.name,
                    u.preferred_username.clone(),
                    u.summary.unwrap_or_default(),
                    crate::to_url(format!("/u/{}/inbox", u.preferred_username)),
                    crate::to_url(format!("/u/{}/outbox", u.preferred_username)),
                    crate::to_url(format!("/u/{}", u.preferred_username)),
                    u.icon_blob.and_then(|s| Uuid::parse_str(&s).ok()),
                    k.to_key().await?,
                )
            ))
        },
        Ok(None) => Err(StatusCode::NOT_FOUND.into()),
        Err(e) => Err(e.into())
    }
}

#[handler]
pub async fn icon(Path(pref_name): Path<String>) -> Response {
    // TODO: use blob instead of raw png
    match tokio::fs::read(format!("./data/icon/{pref_name}.png")).await {
        Ok(bytes) => 
            Response::builder()
                .content_type("image/png")
                .body(bytes)
        ,
        Err(_) => 
            Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(())
    }
}

#[handler]
pub async fn media(Path(uuid): Path<String>) -> Response {
    let Ok(uuid) = Uuid::parse_str(&uuid) else {
        return resp("not found", StatusCode::NOT_FOUND)
    };

    match db::get_blob_by_id(uuid.to_string()).await {
        Ok(data) => {
            // estimate MIME type from binary
            let mime_type = crate::endpoints::try_get_mime(&data.data);
            Response::builder()
                .content_type(mime_type)
                .body(data.data)
        },
        Err(_) => resp("not found", StatusCode::NOT_FOUND)
    }
}

#[handler]
pub async fn activity(Path(id): Path<String>) -> Result<Response> {
    match db::get_activity_by_id(&crate::to_url(format!("/a/{id}"))).await
        .map(
            |a| a.and_then(
                |v| v.try_to_json().ok()
        ))
    {
        Ok(Some(v)) => Ok(to_activity_json(v)),
        Ok(None) => Err(StatusCode::NOT_FOUND.into()),
        Err(_) => Err(StatusCode::BAD_REQUEST.into())
    }
}

#[handler]
pub async fn note(Path(id): Path<String>) -> Result<Response> {
    match db::get_note(&crate::to_url(format!("/a/{id}"))).await {
        Ok(Some(n)) => Ok(to_activity_json(n)),
        Ok(None) => Err(StatusCode::NOT_FOUND.into()),
        Err(_) => Err(StatusCode::BAD_REQUEST.into())
    }
}

// inbox

#[handler]
pub async fn get_inbox(Path(pref_name): Path<String>, query: Query<PagedCollectionQuery>) -> Response {
    log::info!("inbox received: query={:?}", query.0);

    let user_id = match get_uid(&pref_name).await {
        Ok(i) => i, Err(e) => return e,
    };

    let page = query.0.page.unwrap_or(1);
    let (query_params, inbox_entries) = match query.0 {
        x if x.from.is_some() || x.to.is_some() => {
            // if not specified, from is 0(unix time)
            let from = x.from
                .and_then(|s| chrono::NaiveDateTime::parse_from_str(&s, "%s").ok())
                .unwrap_or(chrono::NaiveDateTime::default())
                .and_local_timezone(chrono::Utc)
                .unwrap();
            // if not specified, to is utc::now()
            let to = x.to
                .and_then(|s| chrono::NaiveDateTime::parse_from_str(&s, "%s").ok())
                .unwrap_or(chrono::Utc::now().naive_utc())
                .and_local_timezone(chrono::Utc)
                .unwrap();

            (
                Some(format!("from={}&to={}&", from.format("%s"), to.format("%s"))),
                db::get_user_inbox_by_date(user_id, from, to, page).await
            )
        },
        _ => (None, db::get_user_inbox_with_page(user_id, page).await)
    };
    match inbox_entries {
        Ok(vec) => {
            to_ordered_collection_json::<CollectionItem>(
                page,
                pref_name.clone(),
                crate::to_url(format!("/u/{pref_name}/inbox")),
                query_params,
                vec.into_iter().filter_map(|value| value.try_into().ok()).collect(),
            )
        }
        Err(_) => return resp("database error", StatusCode::INTERNAL_SERVER_ERROR)
    }
}

#[handler]
pub async fn post_inbox(
    Path(pref_name): Path<String>, 
    req: &poem::Request,
    json: Json<serde_json::Value>, 
    header: &HeaderMap
) -> Result<Response> {
    if check_header(header) { 
        let request = json.0.clone();
        let uid = match get_uid(&pref_name).await {
            Ok(i) => i, Err(e) => return Ok(e)
        };

        // verify request that sended from correct user
        if let Err(e) = verify_header(req, &json.0).await {
            log::info!("failed to verify activity: {:?}", e);
            return Err(StatusCode::UNAUTHORIZED.into())
        }

        match json.0.get("type").and_then(|v| v.as_str()) {
            Some(x) if x == "Create" => {
                // create
                log::info!("create activity: {:?}", x);
            },
            Some(x) if x == "Follow" => {
                // follow
                log::info!("follow activity: {:?}", x);
                let _ = inbox::follow(json.0).await;
            },
            Some(x) if x == "Undo" => {
                let Some(object) = json.get("object") else { return Err(StatusCode::BAD_REQUEST.into()) };
                match object.get("type").and_then(|v| v.as_str()) {
                    Some(x) if x == "Follow" => {
                        // unfollow
                        inbox::unfollow(&json.0).await?;
                    },
                    _ => {}
                }
            },
            Some(x) if x == "Accept" => {
                let Some(object) = json.get("object") else { return Err(StatusCode::BAD_REQUEST.into()) };
                match object.get("type") {
                    Some(x) if x.as_str() == Some("Follow") => {
                        // accepted follow
                        inbox::accepted_follow(&json.0).await?;
                    },
                    _ => {}
                }
            },
            _ => return Err(StatusCode::BAD_REQUEST.into())
        };
        db::add_inbox_item(uid, request).await.map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
        Ok(StatusCode::OK.into())
    } else {
        Err(StatusCode::BAD_REQUEST.into()) 
    }
}

// outbox

#[handler]
pub async fn get_outbox(Path(_pref_name): Path<String>) -> Result<StatusCode> {
    Ok(StatusCode::NOT_IMPLEMENTED)
}

#[handler]
pub async fn post_outbox(Path(pref_name): Path<String>, req: Json<serde_json::Value>, cookies: &CookieJar, header: &HeaderMap) -> Result<Response> {
    // check session is valid
    if let Err(e) = check_session(cookies).await {
        return Ok(e)
    }

    if check_header(header) {
        log::info!("post_outbox: header is valid");

        match req.0.get("type").and_then(|v| v.as_str()) {
            // CUD
            Some(x) if 
                x == "Create" ||
                x == "Note"
            => {
                let id = outbox::create(pref_name, &req.0).await?;
                return Ok(
                    Response::builder()
                        .status(StatusCode::CREATED)
                        .header(http::header::LOCATION, id)
                        .finish()
                )
            },
            Some(x) if x == "Update" => {
                return Ok(resp("Unimplemented", StatusCode::NOT_IMPLEMENTED))
            }
            Some(x) if x == "Delete" => {
                return Ok(resp("Unimplemented", StatusCode::NOT_IMPLEMENTED))
            },
            // OTHER
            Some(x) if x == "Follow" => {
                let obj = req.0.get("object")
                    .and_then(|val| val.as_str())
                    .ok_or(StatusCode::BAD_REQUEST)?;
                outbox::follow(obj.into(), pref_name).await?;
            },
            Some(x) if x == "Announce" => {
                return Ok(resp("Unimplemented", StatusCode::NOT_IMPLEMENTED))
            },
            _ => ()
        }
        return Ok(StatusCode::OK.into())
    }
    log::info!("post_outbox: header is invalid");
    Err(StatusCode::NOT_ACCEPTABLE.into())
}

