//! endpoints for /.well-known/...

use http::StatusCode;
use poem::{handler, Result, Response, web::Query};
use serde::Deserialize;
use serde_json::json;

use crate::endpoints::to_activity_json;

#[handler]
pub fn host_meta() -> Response {
    Response::builder()
        .content_type("application/xrd+xml")
        .body(format!(
r#"<?xml version="1.0" encoding="UTF-8"?>
<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
    <Link rel="lrdd" type="application/xrd+xml" template="{}.well-known/webfinger?resource={{uri}}" />
</XRD>"#    ,
            crate::to_url(String::new())
        ))
}

#[derive(Deserialize)]
pub struct WebFingerQuery {
    resource: String
}
#[handler]
pub async fn webfinger(query: Query<WebFingerQuery>) -> Result<Response> {
    let users = crate::db::get_users_list().await?;
    let name_str = query.0.resource
        .replace("acct:", "");
    let formatted = name_str.rsplitn(2, '@').collect::<Vec<_>>();
    
    if let Some(name) = formatted.get(1) {
        if users.contains(&name.to_string()) {
            log::info!("user exist: {}", name);
            let res = json!({
                "subject": format!("acct:{}@{}", name, crate::host()),
                "aliases": [
                    crate::to_url(format!("/u/{name}"))
                ],
                "links" : [
                    {
                        "rel": "self",
                        "type": "application/activity+json",
                        "href": crate::to_url(format!("/u/{name}"))
                    }
                ]
            });
            return Ok(to_activity_json(res));
        }
    }

    log::warn!("user not exist: {}", query.0.resource);
    Err(StatusCode::NOT_FOUND.into())
}

#[handler]
pub fn nodeinfo() -> Response {
    to_activity_json(r#"
{
    "links": [
        {
            "rel": "http://nodeinfo.diaspora.software/ns/schema/2.1",
            "href": "https://ms.puage.org/nodeinfo/2.1"
        }
    ]
}
        "#)
}

#[handler]
pub async fn true_nodeinfo() -> Result<Response> {
    let user_count = crate::db::get_users_count()
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    let version = std::env::var("CARGO_PKG_VERSION").unwrap_or("0.0.0".to_string());

    let info = json!({
        "openRegistrations": false,
        "protocols": [
            "activitypub"
        ],
        "software": {
            "name": "tinyap", 
            "version": version
        },
        "usage": {
            "users": {
                "total": user_count
            }
        },
        "services": {
            "inbound": [],
            "outbound": []
        },
        "metadata": {},
        "version": "2.1"
    });

    Ok(to_activity_json(info))
}