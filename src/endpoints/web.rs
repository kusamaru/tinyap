//! endpoints for web browser access

use std::collections::{HashSet, HashMap};

use futures::{stream, StreamExt};
use once_cell::sync::Lazy;
use poem::{handler, web::Query};
use regex::Regex;
use serde::Deserialize;
use uuid::Uuid;

use crate::{db, structs::NoteObject};

static REGEX_URL: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r#"https?:\/\/[\w\/:%#\$&\?\(\)~\.=\+\-]+"#).unwrap()
});

fn make_hyperlink(s: &str) -> String {
    REGEX_URL.replace_all(
        s, 
        |caps: &regex::Captures<'_>| {
            let link = &caps[0];
            format!(r#"<a class="ext-link" href="{link}">{link}</a>"#)
        }
    ).to_string()
}

fn at_to_prefname(at: &str) -> String {
    at.rsplitn(2, '/').next().map(|s| s.to_string()).unwrap_or_default()
}

#[derive(Deserialize)]
pub struct FeedsQuery {
    page: Option<u32>
}

// TODO: move this const
const CSS: &str = include_str!("../../data/css.css");
#[handler]
pub async fn feeds(query: Query<FeedsQuery>) -> poem::web::Html<String> {
    let page: u32 = query.0.page.unwrap_or(1);
    // TODO!!!!: 複数種類のActivityへの対応
    let Ok(notes) = db::get_all_activities(page).await
        .map(|v| v.into_iter()
            .filter_map(|a| NoteObject::try_from(a).ok()).collect::<Vec<_>>())
    else {
        return poem::web::Html("Failed to get feed".into())
    };
    let mut res_html = String::new();
    res_html.push_str(&format!(
        r#"
<html>
<head>
    <meta charset=UTF-8>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        {}
    </style>
</head>
<body>
    <div>
        <div class="titlebar">
            <h1 class="title">{}: Feed</h1>

            <form class="search" method="GET" action="/search">
                <input class="input" type="search" placeholder="search" name="ct" id="ct" required/>
                <input class="confirm" type="submit" value="search"/>
            </form>
        </div>
        "#, CSS, crate::host()
    ));
    let user_ids = notes.iter().map(|n| n.get_attributed_to()).collect::<HashSet<_>>();
    let users_data = stream::iter(user_ids)
        .filter_map(|at| async move {
            db::get_user_by_pref_name_without_keys(&at_to_prefname(at)).await.ok()
        })
        .then(|u| async move {
            let i = format!(
                "/m/{}", 
                u.icon_blob
                    .clone()
                    .and_then(|s| Uuid::parse_str(&s).ok())
                    .unwrap_or_default()
                    .as_simple()
                );
            (u.preferred_username.clone(), (u, i))
        })
        .collect::<HashMap<_, _>>()
        .await;

    for n in notes {
        let Some((pref_name, icon_src)) = users_data.get(&at_to_prefname(n.get_attributed_to()))
            .map(|(u, i)| (u.preferred_username.clone(), i.clone()))
        else { continue };
        let Ok(attachments) = crate::db::get_attachments_by_note(&n.get_id()).await else { continue };

        res_html.push_str(&format!(r#"
    <div class="note">
        <div class="line">
            <img class="icon" src="{icon_src}" alt="icon of user {pref_name}">
            <a class="prefname" href="/u/{pref_name}">{pref_name}</a>
        </div>
        <p class="content">{}</p>
        <p class="published">{}</p>
        {}
    </div>
    <br>
        "#,
            make_hyperlink(n.get_content()),
            n.get_published(),
            if let Some(medias) = attachments {
                let mut string = String::new();
                medias.iter().for_each(|v| {
                    let uuid = Uuid::parse_str(&v.blob_id).unwrap_or_default();
                    string.push_str(&format!(
                        r#"<img src="{}" alt="image" class="image"></br>"#,
                        crate::to_url(format!("/m/{}", uuid.as_simple().to_string()))
                    ));
                });
                string
            } else {
                String::new()
            }
        ))
    }

    // navigate bar
    res_html.push_str(r#"<div class="navigate-bar">"#);
    // prev
    if page > 1 {
        res_html.push_str(&format!(r#"<a class="prev" href="/?page={}">[PREV PAGE]</a>"#, page-1))
    }

    // next
    if let Ok(true) = db::is_activities_next_page_available(page).await {
        res_html.push_str(&format!(r#"<a class="next" href="/?page={}">[NEXT PAGE]</a>"#, page+1))
    }
    res_html.push_str(r#"</div>"#);

    res_html.push_str(
        r#"
    </div>
</body>
</html>  
        "#
    );

    poem::web::Html(res_html)
}

#[derive(Deserialize)]
pub struct SearchQuery {
    ct: Option<String>,
}
#[handler]
pub async fn search(query: Query<SearchQuery>) -> poem::web::Html<String> {
    let Some(ref ct) = query.ct else {
        return poem::web::Html("query is none...".into())
    };

    let Ok(notes) = db::find_note_by_content(ct).await else {
        return poem::web::Html("query is none...".into())
    };

    let mut res_html = String::new();
    res_html.push_str(&format!(
        r#"
<html>
<head>
    <meta charset=UTF-8>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        {}
    </style>
</head>
<body>
    <div>
        <div class="titlebar">
            <h1 class="title">{}: Search Result</h1>

            <form class="search" method="GET" action="/search">
                <input class="input" type="search" placeholder="search" name="ct" id="ct"/>
                <input class="confirm" type="submit" value="search"/>
            </form>
        </div>
        "#, CSS, crate::host()
    ));

    if let Some(notes) = notes
        .map(|v| v.into_iter()
            .filter_map(|a| NoteObject::try_from(a).ok()).collect::<Vec<_>>())
    {
        let user_ids = notes.iter().map(|n| n.get_attributed_to()).collect::<HashSet<_>>();
        let users_data = stream::iter(user_ids)
            .filter_map(|at| async move {
                db::get_user_by_pref_name_without_keys(&at_to_prefname(at)).await.ok()
            })
            .then(|u| async move {
                let i = format!(
                    "/m/{}", 
                    u.icon_blob
                        .clone()
                        .and_then(|s| Uuid::parse_str(&s).ok())
                        .unwrap_or_default()
                        .as_simple()
                    );
                (u.preferred_username.clone(), (u, i))
            })
            .collect::<HashMap<_, _>>()
            .await;

        for n in notes {
            let Some((pref_name, icon_src)) = users_data.get(&at_to_prefname(&n.get_attributed_to()))
                .map(|(u, i)| (u.preferred_username.clone(), i.clone()))
            else { continue };
            let Ok(attachments) = crate::db::get_attachments_by_note(&n.get_id()).await else { continue };

            res_html.push_str(&format!(r#"
        <div class="note">
            <div class="line">
                <img class="icon" src="{icon_src}" alt="icon of user {pref_name}">
                <a class="prefname" href="/u/{pref_name}">{pref_name}</a>
            </div>
            <p class="content">{}</p>
            <p class="published">{}</p>
            {}
        </div>
        <br>
            "#,
                make_hyperlink(n.get_content()),
                n.get_published(),
                if let Some(medias) = attachments {
                    let mut string = String::new();
                    medias.iter().for_each(|v| {
                        let uuid = Uuid::parse_str(&v.blob_id).unwrap_or_default();
                        string.push_str(&format!(
                            r#"<img src="{}" alt="image" class="image"></br>"#,
                            crate::to_url(format!("/m/{}", uuid.as_simple().to_string()))
                        ));
                    });
                    string
                } else {
                    String::new()
                }
            ))
        }
    } else {
        res_html.push_str("not found<br>")
    }

    res_html.push_str(
        r#"
    <div>
        <a href="/">[RETURN TO MAIN FEED]</a>
    </div>
        "#
    );

    res_html.push_str(
        r#"
    </div>
</body>
</html>  
        "#
    );

    poem::web::Html(res_html)
}