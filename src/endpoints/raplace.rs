//! raplace api endpoints

use http::{HeaderMap, StatusCode};
use poem::{handler, web::{Json, cookie::{CookieJar, Cookie}, Query, Multipart}, Response};
use serde::{Deserialize, Serialize};

use crate::{db, structs::PagedCollectionQuery, endpoints::{get_uid, resp, SESSION_ID_COOKIE, check_session, to_ordered_collection_json, is_mime_media, try_get_mime}, auth::verify_password};

#[derive(Deserialize)]
pub struct LoginRequest {
    username: String,
    password: String,
}
#[handler]
pub async fn login(Json(req): Json<LoginRequest>, headers: &HeaderMap, cookies: &CookieJar) -> Response {
    log::info!("login request received");

    let user_id = match get_uid(&req.username).await {
        Ok(i) => i, Err(e) => return e,
    };

    let Ok(expected) = db::get_user_creds(user_id).await else {
        return resp("unknown error: login fail", StatusCode::INTERNAL_SERVER_ERROR)
    };
    if let Err(_) = verify_password(req.password, expected.passhash) {
        return resp("login failed", StatusCode::BAD_REQUEST);
    }

    let session_id = uuid::Uuid::new_v4().to_string();
    let expires_duration = std::time::Duration::from_secs(crate::CONFIG.get().unwrap().session_expires);
    let expires = chrono::Utc::now() + chrono::Duration::from_std(expires_duration).unwrap();
    let user_agent = headers.get(http::header::USER_AGENT)
        .and_then(|v| v.to_str().ok())
        .map(|s| s.to_string());
    match db::add_session(&session_id, user_id, expires, user_agent).await {
        Ok(_) => {
            let mut cookie = Cookie::new(SESSION_ID_COOKIE, session_id);
            cookie.set_domain(crate::host());
            cookie.set_http_only(true);
            cookie.set_secure(true);
            cookie.set_expires(expires);
            cookies.add(cookie);
            resp("", StatusCode::OK)
        },
        Err(_) => resp("login failed", StatusCode::INTERNAL_SERVER_ERROR)
    }
}

/// get feed data. requires authentication
#[handler]
pub async fn get_feed(query: Query<PagedCollectionQuery>, cookies: &CookieJar) -> Response {
    // check session is valid
    if let Err(e) = check_session(cookies).await {
        return e
    }

    // get userdata
    let Some(id_cookie) = cookies.get(SESSION_ID_COOKIE) else {
        return resp("session_id not found", StatusCode::UNAUTHORIZED)
    };
    let session_id = id_cookie.value_str();

    let Ok(crate::structs::DbUser { id, preferred_username, .. }) = db::get_user_by_session(session_id).await else {
        return resp("bad session", StatusCode::UNAUTHORIZED)
    };

    // query paramaters
    let from = query.0.from.clone()
        .and_then(|s| chrono::NaiveDateTime::parse_from_str(&s, "%s").ok())
        .unwrap_or(chrono::NaiveDateTime::default())
        .and_local_timezone(chrono::Utc)
        .unwrap();
    let to = query.0.to.clone()
        .and_then(|s| chrono::NaiveDateTime::parse_from_str(&s, "%s").ok())
        .unwrap_or(chrono::Utc::now().naive_utc())
        .and_local_timezone(chrono::Utc)
        .unwrap();
    let page = query.0.page.unwrap_or(1);

    // get user feed data
    let query_params = format!("from={}&to={}&", from.format("%s"), to.format("%s"));
    let entries = 
        db::get_posted_activities_and_received_activities(id, from, to, page).await;

    let result = match entries {
        Ok(vec) => vec,
        Err(e) => {
            log::error!("{}", e.to_string());
            return resp("database error", StatusCode::INTERNAL_SERVER_ERROR)
        }
    };

    to_ordered_collection_json::<serde_json::Value>(
        page,
        preferred_username,
        crate::to_url("/a/getfeed".into()),
        Some(query_params),
        result.iter().filter_map(|v| v.try_to_json().ok()).collect::<_>(),
    )
}


#[derive(Serialize)]
pub struct MediaUploadResult {
    pub result: Vec<MediaUploadKind>,
}
#[derive(Serialize, Debug)]
#[serde(tag = "type")]
pub enum MediaUploadKind {
    Success {
        name: String,
        available_on: String,
    },
    Failure {
        name: String,
        reason: String,
    }
}

/// Receive media(s) by multipart. requires authentication
#[handler]
pub async fn upload_media(mut data: Multipart, cookies: &CookieJar) -> Response {
    // authentication
    if let Err(e) = check_session(cookies).await {
        log::warn!("authentication failed");
        return e
    }

    // get userdata
    let Some(id_cookie) = cookies.get(SESSION_ID_COOKIE) else {
        return resp("session_id not found", StatusCode::UNAUTHORIZED)
    };
    let session_id = id_cookie.value_str();

    let Ok(crate::structs::DbUser { id, .. }) = db::get_user_by_session(session_id).await else {
        return resp("bad session", StatusCode::UNAUTHORIZED)
    };

    // insert to db
    let mut result: Vec<MediaUploadKind> = Vec::new();


    while let Ok(Some(field)) = data.next_field().await {
        let Some(name) = field.name().map(|s| s.to_string()) else { continue };
        log::info!("name: {}", name);    
        
        let field_type = field.content_type().map(|s| s.to_string());
        let Ok(mut bytes) = field.bytes().await else {
            result.push(MediaUploadKind::Failure { name, reason: String::from("Could not read bytes") });
            continue;
        };

        // TODO: 
        let mime_type = field_type.unwrap_or_else(|| try_get_mime(&bytes));
        if !is_mime_media(&mime_type) {
            log::error!("{mime_type}");
            result.push(MediaUploadKind::Failure { name, reason: String::from("File is not media") });
            continue;
        }

        // TODO: 
        let Ok(parsed) = mime_type.parse::<mime::Mime>() else {
            result.push(MediaUploadKind::Failure { name, reason: String::from("Failed to parse MIME type") });
            continue;
        };

        let processed_bytes = match parsed.type_() {
            mime::IMAGE => {
                let name_cloned = name.clone();
                // compress image
                // run on another thread to avoid blocking in main thread
                match tokio::task::spawn_blocking(move || {
                    let Ok(data) = crate::conv::convert_to_webp(&bytes) else {
                        return Err(MediaUploadKind::Failure { name: name_cloned, reason: String::from("Failed to compress image") });
                    };
                    Ok(data.to_vec())
                }).await {
                    Ok(Ok(data)) => data,
                    Ok(Err(e)) => {
                        result.push(e);
                        continue;
                    },
                    e => {
                        log::error!("Unexcepted Error: {:?}", e);
                        result.push(MediaUploadKind::Failure { name, reason: String::from("Unexcepted Error") });
                        continue;
                    },
                }
            },
            _ => bytes
        };

        let Ok(uuid) = db::insert_blob(&processed_bytes, id).await else {
            result.push(MediaUploadKind::Failure { name, reason: String::from("Database error") });
            continue;
        };

        // success
        result.push(MediaUploadKind::Success { 
            name, 
            available_on: crate::to_url(format!("/m/{}", uuid.as_simple()))
        });
    }

    log::info!("Media upload result: {:?}", result);

    Response::builder()
        .header(http::header::CONTENT_TYPE, "application/json")
        .body(serde_json::to_string_pretty(&MediaUploadResult { result }).unwrap())
}

#[derive(Deserialize)]
pub struct UpdateUserInfoRequest {
    name: Option<String>,
    summary: Option<String>,
    icon_blob: Option<String>,
}
#[handler]
pub async fn update_user_info(Json(mut req): Json<UpdateUserInfoRequest>, cookies: &CookieJar) -> Response {
    // authentication
    if let Err(e) = check_session(cookies).await {
        log::warn!("authentication failed");
        return e
    }

    // get current userdata
    let Some(id_cookie) = cookies.get(SESSION_ID_COOKIE) else {
        return resp("session_id not found", StatusCode::UNAUTHORIZED)
    };
    let session_id = id_cookie.value_str();

    let Ok(mut db_user) = db::get_user_by_session(session_id).await else {
        return resp("bad session", StatusCode::UNAUTHORIZED)
    };

    // update
    if let Some(n) = req.name { db_user.name = n }
    db_user.summary = db_user.summary.or(req.summary);
    db_user.icon_blob = db_user.icon_blob.or(req.icon_blob);

    match db::update_user(db_user).await {
        Ok(_) => resp("", StatusCode::OK),
        Err(_) => resp("database error", StatusCode::INTERNAL_SERVER_ERROR),
    }
}

#[derive(Deserialize)]
pub struct SearchPostsQuery {
    ct: Option<String>,
    page: Option<u32>,
}

/// search post(s) by using query params. requires authentication (may change)
#[handler]
pub async fn search_posts(query: Query<SearchPostsQuery>, cookies: &CookieJar) -> Response {
    // check session is valid
    if let Err(e) = check_session(cookies).await {
        return e
    }

    // get userdata
    let Some(id_cookie) = cookies.get(SESSION_ID_COOKIE) else {
        return resp("session_id not found", StatusCode::UNAUTHORIZED)
    };
    let session_id = id_cookie.value_str();

    let Ok(crate::structs::DbUser { preferred_username, .. }) = db::get_user_by_session(session_id).await else {
        return resp("bad session", StatusCode::UNAUTHORIZED)
    };

    let ct = query.0.ct.unwrap_or_default();
    let page = query.0.page.unwrap_or(1);

    // get user feed data
    let query_params = format!("ct={ct}");
    let entries = 
        db::find_note_by_content(&ct).await;

    let result = match entries {
        Ok(vec) => vec.unwrap_or(vec![]),
        Err(e) => {
            log::error!("{}", e.to_string());
            return resp("database error", StatusCode::INTERNAL_SERVER_ERROR)
        }
    };

    to_ordered_collection_json::<serde_json::Value>(
        page,
        preferred_username,
        crate::to_url("/a/search".into()),
        Some(query_params),
        result.iter().filter_map(|v| v.try_to_json().ok()).collect::<_>(),
    )
}
