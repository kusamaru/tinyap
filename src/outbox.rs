use chrono::Utc;
use http::StatusCode;

use crate::{db::{self, get_followers_inboxes}, structs::MediaObject, response};

pub async fn create(pref_name: String, req: &serde_json::Value) -> Result<String, StatusCode> {
    let Ok(Some((user, _))) = db::get_user_by_pref_name(&pref_name).await else {
        return Err(StatusCode::UNAUTHORIZED)
    };

    match req.get("type").and_then(|v| v.as_str()) {
        Some("Note") => {
            // validate
            if !matches!(req.get("@context"), Some(x) if x == "https://www.w3.org/ns/activitystreams") {
               return Err(StatusCode::BAD_REQUEST)
            }
            let content = req.get("content")
                .and_then(|v| v.as_str())
                .ok_or(StatusCode::BAD_REQUEST)?;
            let in_reply_to = req.get("inReplyTo")
                .and_then(|v| v.as_str())
                .map(|s| s.to_string());
            let attachment = req.get("attachment")
                .and_then(|v| v.as_array())
                .map(|vec| vec.iter()
                    .filter_map(|value| serde_json::from_value::<MediaObject>(value.clone()).ok())
                    .collect());

            let note_id = match db::insert_note(
                &user.preferred_username, 
                content,
                user.timezone,  
                Utc::now().naive_utc(),
                None,
                in_reply_to,
                attachment
            ).await {
                Ok(id) => id,
                Err(e) => {
                    log::error!("noteactivity error: {}", e.to_string());
                    return Err(StatusCode::INTERNAL_SERVER_ERROR)
                }
            };
            log::info!("note was successfully created. \nid is {note_id}");
            // post to followers inbox
            let na = db::get_note(&note_id).await
                .ok().flatten()
                .ok_or_else(|| {
                    log::error!("Error: could not find note {note_id}");
                    StatusCode::INTERNAL_SERVER_ERROR
                })?;

            let inboxes = get_followers_inboxes(user.id).await
                .map_err(|_| StatusCode::NOT_FOUND)?;

            if let Err(e) = response::create(
                inboxes, 
                &user.preferred_username, 
                na
            ).await {
                log::error!("database error: {}", e.to_string());
                return Err(StatusCode::INTERNAL_SERVER_ERROR)
            }

            return Ok(note_id)
        },
        Some("Create") => {
            // unimplemented
            return Err(StatusCode::NOT_IMPLEMENTED)
        }
        _ => return Err(StatusCode::NOT_ACCEPTABLE)
    }
}

pub async fn follow(target_user_id: String, pref_name: String) -> Result<(), StatusCode> {
    let data = crate::get_actor_data(&target_user_id).await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    let inbox = data.get("inbox")
        .and_then(|val| val.as_str())
        .map(|s| s.to_string())
        .ok_or(StatusCode::INTERNAL_SERVER_ERROR)?;

    let name = data.get("name")
        .and_then(|val| val.as_str())
        .map(|s| s.to_string());

    let shared_inbox = data.get("sharedInbox")
        .and_then(|val| val.as_str())
        .map(|s| s.to_string());

    response::follow(&inbox, &pref_name, &target_user_id).await.map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    match db::insert_followee(
        pref_name,
        target_user_id.into(),
        name,
        Some(inbox),
        shared_inbox
    ).await {
        Ok(_) => Ok(()),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR)
    }
}