use image::{GenericImageView, imageops, DynamicImage};
use webp::*;

const TARGET_SIZE: u32 = 1600;

pub fn convert_to_webp(bytes: &[u8]) -> Result<WebPMemory, Box<dyn std::error::Error>> {
    let img = resize(bytes)?;
    let encoder = Encoder::from_image(&img)?;
    Ok(encoder.encode(85.0))
}

fn resize(bytes: &[u8]) -> Result<DynamicImage, image::ImageError> {
    let img = image::load_from_memory(bytes)?;
    let (w, h) = img.dimensions();

    if w > TARGET_SIZE || h > TARGET_SIZE {
        let (tw,th) = 
            if w > h {
                let ratio: f32 = TARGET_SIZE as f32 / w as f32;
                (TARGET_SIZE, (h as f32 * ratio) as u32)
            } else {
                let ratio: f32 = TARGET_SIZE as f32 / h as f32;
                ((w as f32 * ratio) as u32, TARGET_SIZE)
            };
        let resized = img.resize(
            tw, 
            th,
            imageops::FilterType::Triangle
        );
        Ok(resized)
    } else {
        Ok(img)
    }
}