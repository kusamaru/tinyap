mod db;
mod endpoints;
mod inbox;
mod structs;

mod api;
mod auth;
mod config;
mod conv;
mod outbox;
mod response;
mod route;
mod web;

use config::GeneralConfig;
use once_cell::sync::OnceCell;

const ACT_JSON: &str = "application/activity+json";
const TRUE_TYPE: &str = r#"application/ld+json; profile="https://www.w3.org/ns/activitystreams""#;

static ADMIN_KEY: OnceCell<crate::structs::DbKey> = OnceCell::new();
pub fn get_admin_key() -> Vec<u8> {
    ADMIN_KEY.get().unwrap().secret.as_bytes().to_vec()
}
static CONFIG: OnceCell<GeneralConfig> = OnceCell::new();
pub fn host<'a>() -> &'a str { &CONFIG.get().unwrap().host_name }
fn addr_port() -> String { 
    let cfg = CONFIG.get().unwrap();  
    format!("{}:{}", cfg.publish_addr, cfg.publish_port)
}

#[cfg(not(feature="http"))]
const SCHEME: &str = "https";
#[cfg(feature="http")]
const SCHEME: &str = "http";

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    // initialize
    init().await?;

    #[cfg(feature="debug")]
    {
        log::info!("running as debug-mode.");
        let users_list = db::get_users_list().await;
        log::info!("users_list: {:?}", users_list);
    }

    let server = 
        if cfg!(feature = "http") { 
            // HTTP
            crate::web::run(
                addr_port(),
                "".into(),
                "".into()
            )
        } else {
            // HTTPS
            crate::web::run(
                addr_port(),
                std::env::var("TLS_CERT_PATH").unwrap(), 
                std::env::var("TLS_KEY_PATH").unwrap(),
            )
        };
    let session_manager = auth::run_session_manager();

    match tokio::join!(server, session_manager) {
        (Ok(_), Ok(_)) => Ok(()),
        (Err(e), _) => panic!("Server Error: {}", e.to_string()),
        (_, Err(e)) => panic!("Session Manager Error: {}", e.to_string()),
    }
}

async fn init() -> anyhow::Result<()> {
    #[cfg(not(feature="debug"))]
    std::env::set_var("RUST_LOG", "info");
    #[cfg(feature="debug")]
    std::env::set_var("RUST_LOG", "debug");
    env_logger::init();

    let _ = CONFIG.set(crate::config::load_general_config()?);
    crate::db::init().await?;
    let _ = ADMIN_KEY.set(crate::db::get_admin_keypair().await?);

    Ok(())
}

pub async fn get_actor_data(actor: &str) -> anyhow::Result<serde_json::Value> {
    let mut request = http::Request::get(actor)
        .header(http::header::ACCEPT, format!("{ACT_JSON}, {TRUE_TYPE}"))
        .body("")?;
    auth::sign_by_admin(&mut request, actor)?;
    let res = reqwest::Client::new()
        .execute(request.try_into().unwrap())
        .await?;
    if res.status().as_u16() >= 400 && res.status().as_u16() < 600 {
        // Err
        anyhow::bail!("get_actor_data: {:?}", res.text().await)
    }
    let json = res.json::<serde_json::Value>().await?;
    Ok(json)
}

fn get_name_from_url(url: &str) -> Option<String> {
    let user_url = url.rsplitn(2, "/").collect::<Vec<_>>();
    if let Some(s) = user_url.get(0) { Some(s.to_string()) } else { None }
}

/// format '/hogehoge/fugafuga/aaa' to '{SCHEME}://{HOST}/hogehoge/fugafuga/aaa'
fn to_url(path: String) -> String {
    format!(
        "{}://{}{}{}", 
        crate::SCHEME,
        crate::host(),
        if !path.starts_with("/") { "/" } else { "" },
        path
    )
}