use anyhow::Context;
use sqlx::{Sqlite, Database, Executor};
use uuid::Uuid;

use crate::structs::{DbAttachment, MediaObject};

use super::get_db;

pub async fn get_attachments_by_note(object_id: &str) -> anyhow::Result<Option<Vec<DbAttachment>>> {
    let pool = get_db();
    let attachment = sqlx::query_as!(
        DbAttachment,
        r#"
select * from attachments
where object_id = ?
        "#,
        object_id)
        .fetch_all(pool)
        .await;
    
    match attachment {
        Ok(v) => Ok(Some(v)),
        Err(sqlx::Error::RowNotFound) => Ok(None),
        Err(e) => Err(anyhow::anyhow!(e))
    }
}

pub async fn insert_attachments(object_id: &str, media: &Vec<MediaObject>) -> anyhow::Result<()> {
    let mut tran = get_db().begin().await?;

    for m in media {
        insert_attachment(&mut *tran, object_id, m).await?;

        let media_id = m.url.rsplitn(2, "/").next().context("media is not valid")?;
        let blob_id = Uuid::parse_str(media_id)?.to_string();
        sqlx::query!(
            r#"
insert into attachments ( object_id, blob_id )
values ( ?, ? )
            "#,
            object_id,
            blob_id,)
            .execute(&mut *tran)
            .await?;
    }

    tran.commit().await?;
    Ok(())
}

pub async fn insert_attachment<'a, E>(tran: &'a mut E, object_id: &str, media: &MediaObject) -> anyhow::Result<()> 
where &'a mut E: sqlx::Executor<'a, Database = Sqlite> {
    let media_id = media.url.rsplitn(2, "/").next().context("media is not valid")?;
    let blob_id = Uuid::parse_str(media_id)?.to_string();
    sqlx::query!(
        r#"
insert into attachments ( object_id, blob_id )
values ( ?, ? )
        "#,
        object_id,
        blob_id,)
        .execute(tran)
        .await?;

    Ok(())
}

pub async fn get_attachments_by_ids(ids: &[&str]) -> anyhow::Result<Vec<DbAttachment>> {
    let pool = get_db();

    let mut result: Vec<DbAttachment> = Vec::new();
    for object_id in ids {
        let Ok(mut attach) = sqlx::query_as!(
            DbAttachment,
            r#"
select * from attachments
where object_id = ?
            "#,
            object_id)
            .fetch_all(pool)
            .await
        else {
            // any error.
            continue;
        };
        
        result.append(&mut attach)
    }

    Ok(result)
}