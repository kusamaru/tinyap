use crate::{structs::DbFollower, db::{get_uid, is_external_user_exists, insert_external_user}};

use super::get_db;

/// get followers list order by followed_at(ascending).
pub async fn get_followers(user_id: i64) -> anyhow::Result<Vec<DbFollower>> {
    let pool = get_db();
    let record = sqlx::query_as!(
        DbFollower,
        r#"
select * from followers
where user_id = ?
order by followed_at
        "#,
        user_id)
        .fetch_all(pool)
        .await?;
    Ok(record)
}

pub async fn get_followers_inboxes(user_id: i64) -> anyhow::Result<Vec<String>> {
    let pool = get_db();
    let record = sqlx::query!(
        r#"
select inbox, shared_inbox from external_users
where exists (
    select * from followers
    where external_users.id = followers.follower_id
      and followers.user_id = ?
)
        "#,
        user_id)
        .fetch_all(pool)
        .await?;
    let res = record.into_iter()
        .filter_map(|r| r.shared_inbox.or(r.inbox))
        .collect::<Vec<String>>();
    Ok(res)
}

pub async fn insert_follower(pref_name: String, follower_id: String, name: Option<String>, inbox: Option<String>, shared_inbox: Option<String>) -> anyhow::Result<()> {
    log::info!("insert_follower: pref_name={}, follower_id={}, follower_name={:?}, follower_inbox={:?}", pref_name, follower_id, name, inbox);

    let uid = match get_uid(&pref_name).await? {
        Some(i) => i, None => anyhow::bail!("user not found")
    };
    let mut tran = get_db().begin().await?;
    // checking external user is exist
    if !is_external_user_exists(&follower_id).await? {
        insert_external_user(&follower_id, name, inbox, shared_inbox, None).await;
    }

    // insert
    sqlx::query!(
        r#"
insert into followers (
    user_id, 
    follower_id
) values (
    ?,
    ?
)
        "#,
        uid,
        follower_id)
        .execute(&mut *tran)
        .await?;
    tran.commit().await?;

    Ok(())
}

pub async fn remove_follower(pref_name: String, follower_id: String) -> anyhow::Result<()> {
    log::info!("remove_follower: pref_name={}, follower_id={}", pref_name, follower_id);

    let uid = match get_uid(&pref_name).await? {
        Some(i) => i, None => anyhow::bail!("user not found")
    };
    let mut tran = get_db().begin().await?;
    sqlx::query!(
        r#"
delete from followers
where user_id = ? and follower_id = ?
        "#,
        uid,
        follower_id)
        .execute(&mut *tran)
        .await?;
    tran.commit().await?;

    Ok(())
}