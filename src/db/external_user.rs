use anyhow::Context;
use log::info;

use crate::structs::DbExternalUser;

use super::get_db;

#[async_recursion::async_recursion]
pub async fn get_external_user(user_id: &str) -> anyhow::Result<DbExternalUser> {
    let pool = get_db();

    let record = sqlx::query_as!(
        DbExternalUser,
        r#"
select * from external_users
where id = ?
        "#,
        user_id)
        .fetch_one(pool)
        .await;

    match record {
        Ok(u) => return Ok(u),
        Err(sqlx::Error::RowNotFound) => {
            let data = crate::get_actor_data(&user_id).await?;
            let id = data.get("id")
                .and_then(|val| val.as_str())
                .context("user id is invalid")?;
            let inbox = data.get("inbox")
                .and_then(|val| val.as_str())
                .map(|s| s.to_string());
            let name = data.get("name")
                .and_then(|val| val.as_str())
                .map(|s| s.to_string());
            let shared_inbox = data.get("sharedInbox")
                .and_then(|val| val.as_str())
                .map(|s| s.to_string());
            let publickey = data.get("publicKey")
                .and_then(|val| val.get("publicKeyPem"))
                .and_then(|val| val.as_str())
                .map(|s| s.to_string());

            if let Err(e) = insert_external_user(id, name, inbox, shared_inbox, publickey).await {
                return Err(e)
            }
            Ok(get_external_user(user_id).await?)
        },
        Err(e) => return Err(e.into())
    }
}

pub async fn insert_external_user(user_id: &str, name: Option<String>, inbox: Option<String>, shared_inbox: Option<String>, publickey: Option<String>) -> anyhow::Result<()> {
    let mut tran = get_db().begin().await?;
    sqlx::query!(
        r#"
insert into external_users (
    id, name, inbox, shared_inbox, publickey
) values (
    ?, ?, ?, ?, ?
)
        "#,
        user_id,
        name,
        inbox,
        shared_inbox,
        publickey)
        .execute(&mut *tran)
        .await?;
    tran.commit().await?;
    Ok(())
}

pub async fn is_external_user_exists(external_user_id: &str) -> anyhow::Result<bool> {
    let pool = get_db();
    let res = sqlx::query!(
        r#"
select id from external_users
where id = ?
        "#,
        external_user_id)
        .fetch_one(pool)
        .await;
    match res {
        Ok(_) => Ok(true),
        Err(sqlx::Error::RowNotFound) => Ok(false),
        Err(e) => Err(e.into())
    }
}

pub async fn update_publickey(user_id: &str, publickey: &str) -> anyhow::Result<()> {
    let mut tran = get_db().begin().await?;
    sqlx::query!(
        r#"
update external_users
set publickey = ?
where id = ?
        "#,
        publickey,
        user_id)
        .execute(&mut *tran)
        .await?;
    tran.commit().await?;
    info!("user '{}' publickey is successfully updated", user_id);
    Ok(())
}