use chrono::Utc;

use crate::structs::DbActivity;

use super::{PAGE_SIZE, get_db};

pub async fn get_user_inbox_with_page(user_id: i64, page: u32) -> anyhow::Result<Vec<DbActivity>> {
    let pool = get_db();
    let offset = (page - 1) * PAGE_SIZE;

    let inbox_page = sqlx::query_as!(
        DbActivity,
        r#"
select * from activities
where 
    EXISTS(
        select * from activities_received_users
        where received_user_id = ?
    )
order by JSON_EXTRACT(object_json, '$.published') desc
limit ?, ?
        "#,
        user_id,
        offset,
        PAGE_SIZE)
        .fetch_all(pool)
        .await?;
    
    Ok(inbox_page)
}

pub async fn get_user_inbox_by_date(user_id: i64, from: chrono::DateTime<Utc>, to: chrono::DateTime<Utc>, page: u32) -> anyhow::Result<Vec<DbActivity>> {
    let pool = get_db();
    let offset = (page - 1) * PAGE_SIZE;

    let inbox_page = sqlx::query_as!(
        DbActivity,
        r#"
select * from activities
where 
    datetime(JSON_EXTRACT(object_json, '$.published')) between ? and ?
    AND
    EXISTS(
        select * from activities_received_users
        where received_user_id = ?
    )
order by JSON_EXTRACT(object_json, '$.published') desc
limit ?, ?
        "#,
        from,
        to,
        user_id,
        offset,
        PAGE_SIZE)
        .fetch_all(pool)
        .await?;

    Ok(inbox_page)
}

pub async fn add_inbox_item(user_id: i64, item: serde_json::Value) -> anyhow::Result<()> {
    let object_json = item.to_string();
    let object_id = item.get("id")
        .and_then(|v| v.as_str())
        .ok_or(anyhow::anyhow!("id not found"))?;
    let mut tran = get_db().begin().await?;
    sqlx::query!(
        r#"
insert into activities (
    object_id, object_json
) VALUES (
    ?, ?
)
        "#,
        object_id,
        object_json)
        .execute(&mut *tran)
        .await?;
    tran.commit().await?;
    Ok(())
}

