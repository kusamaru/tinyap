use crate::{structs::{DbUser, DbActivity}, to_url};

use super::{get_db, get_user_by_id};

pub async fn get_url(user_id: i64, user_note_id: i64) -> anyhow::Result<String> {
    let DbUser { preferred_username: name, .. } = get_user_by_id(user_id).await?;
    Ok(to_url(format!("/u/{name}/note/{user_note_id}")))
}

async fn get_replies(url: &str) -> anyhow::Result<Vec<DbActivity>> {
    let pool = get_db();

    let result: Vec<DbActivity> = sqlx::query_as!(
        DbActivity,
        r#"
select * from activities
where JSON_EXTRACT(object_json, '$.inReplyTo') = ?
        "#,
        url)
        .fetch_all(pool)
        .await?;

    Ok(result)
}