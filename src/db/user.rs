use crate::structs::{DbUser, DbKey};

use super::{get_or_create_keypair, get_db};

pub async fn get_users_list() -> anyhow::Result<Vec<String>> {
    let pool = get_db();
    let record = sqlx::query!(
        r#"
select preferred_username from users
        "#
    )
    .fetch_all(pool)
    .await;

    match record {
        Ok(r) => Ok(r.iter().map(|r| r.preferred_username.to_string()).collect()),
        Err(sqlx::Error::RowNotFound) => Ok(Vec::new()),
        Err(e) => Err(e.into()),
    }
}

pub async fn get_uid(pref_name: &String) -> anyhow::Result<Option<i64>> {
    let pool = get_db();
    let uid = sqlx::query!(
        r#"
select id from users
where preferred_username = ?
        "#,
        pref_name)
        .fetch_one(pool)
        .await;
    match uid {
        Ok(r) => Ok(Some(r.id)),
        Err(sqlx::Error::RowNotFound) => Ok(None),
        Err(e) => Err(e.into()),
    }
}

pub async fn get_user_by_pref_name(pref_name: &String) -> anyhow::Result<Option<(DbUser, DbKey)>> {
    let pool = get_db();
    let user = match sqlx::query_as!(
        DbUser,
        r#"
select * from users 
where preferred_username = ?
        "#,
        pref_name
    ).fetch_one(pool).await {
        Ok(u) => u,
        Err(sqlx::Error::RowNotFound) => return Ok(None),
        Err(e) => return Err(anyhow::anyhow!(e)),
    };
    match get_or_create_keypair(user.id).await {
        Ok(key) => Ok(Some((user, key))),
        Err(e) => Err(anyhow::anyhow!(e)),
    }
}

pub async fn get_user_by_pref_name_without_keys(pref_name: &String) -> anyhow::Result<DbUser> {
    let pool = get_db();
    
    match sqlx::query_as!(
        DbUser,
        r#"
select * from users 
where preferred_username = ?
        "#,
        pref_name
    ).fetch_one(pool).await {
        Ok(u) => Ok(u),
        Err(e) => Err(anyhow::anyhow!(e)),
    }
}

pub async fn get_user_by_id(id: i64) -> anyhow::Result<DbUser> {
    let pool = get_db();
    let rcd = sqlx::query_as!(
        DbUser,
        r#"
select * from users 
where id = ?
        "#,
        id)
        .fetch_one(pool)
        .await;
    match rcd {
        Ok(user) => Ok(user),
        Err(e) => Err(anyhow::anyhow!(e)),
    }
}

pub async fn get_user_by_session(session_id: &str) -> anyhow::Result<DbUser> {
    let pool = get_db();
    let user = sqlx::query_as!(
        DbUser,
        r#"
select * from users
where id = (
    select user_id from sessions
    where session_id = ?
)
        "#,
        session_id)
        .fetch_one(pool)
        .await;

    match user {
        Ok(u) => Ok(u),
        Err(e) => {
            log::error!("{}", e.to_string());
            Err(anyhow::anyhow!(e))
        }
    }
}

pub async fn insert_user(pref_name: String, name: String, timezone: Option<i64>, summary: Option<String>) -> anyhow::Result<()> {
    let mut tran = get_db().begin().await?;
    sqlx::query!(
        r#"
insert into users ( preferred_username, name, summary, timezone )
values ( ?, ?, ?, ? )
        "#,
        pref_name,
        name,
        summary,
        timezone)
        .execute(&mut *tran)
        .await?;
    tran.commit().await?;
    Ok(())
} 

pub async fn update_user(user: DbUser) -> anyhow::Result<()> {
    let mut tran = get_db().begin().await?;
    
    sqlx::query!(
        r#"
update users
set 
    name = ?,
    summary = ?,
    icon_blob = ?
where
    id = ?
        "#,
        user.name,
        user.summary,
        user.icon_blob,
        user.id)
        .execute(&mut *tran)
        .await?;

    tran.commit().await?;
    Ok(())
}

pub async fn get_users_count() -> anyhow::Result<i32> {
    let pool = get_db();

    let result = sqlx::query!(r#"select count(id) as count from users"#)
        .fetch_one(pool)
        .await;

    match result {
        Ok(record) => Ok(record.count),
        Err(e) => {
            log::error!("{}", e.to_string());
            Err(e.into())
        }
    }
}