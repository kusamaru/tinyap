use uuid::Uuid;

use crate::structs::DbBlob;

use super::get_db;

pub async fn get_blob_by_id(id: String) -> anyhow::Result<DbBlob> {
    let pool = get_db();

    let blob = sqlx::query_as!(
        DbBlob,
        r#"
select * from blobs
where id = ?
        "#,
        id)
        .fetch_one(pool)
        .await?;
    Ok(blob)
}

pub async fn insert_blob(data: &[u8], uploaded_by: i64) -> anyhow::Result<Uuid> {
    let mut tran = get_db().begin().await?;

    let id = Uuid::new_v4();
    let str_id = id.to_string();
    sqlx::query!(
        r#"
insert into blobs ( id, uploaded_by, data )
values ( ?, ?, ? )
        "#,
        str_id,
        uploaded_by,
        data)
        .execute(&mut *tran)
        .await?;

    tran.commit().await?;
    Ok(id)
}

pub async fn remove_blob(id: Uuid) -> anyhow::Result<()> {
    let mut tran = get_db().begin().await?;

    sqlx::query!(
        r#"
delete from blobs
where id = ?
        "#,
        id)
        .execute(&mut *tran)
        .await?;

    tran.commit().await?;
    Ok(())
}