use crate::structs::{NoteObject, MediaObject, DbActivity};

use super::get_db;


pub async fn get_note(id: &str) -> anyhow::Result<Option<NoteObject>> {
    let pool = get_db();

    // TODO: may fail
    let note = sqlx::query_as!(
        DbActivity,
        r#"
select * from activities
where JSON_EXTRACT(object_json, '$.type') = ? AND object_id = ?
        "#,
        "Note",
        id)
        .fetch_one(pool)
        .await;

    match note.map(|a| a.try_into()) {
        Ok(Ok(n)) => Ok(Some(n)),
        Ok(Err(e)) => {
            log::error!("Serialize Error: {}", e.to_string());
            Err(anyhow::anyhow!(e))
        }
        Err(e) => {
            log::error!("Database Error: {}", e.to_string());
            Err(anyhow::anyhow!(e))
        }
    }
}

pub async fn insert_note(
    pref_name: &str,
    content: &str,
    timezone: Option<i64>,
    published: chrono::NaiveDateTime,
    to: Option<Vec<String>>,
    in_reply_to: Option<String>,
    attachments: Option<Vec<MediaObject>>
) -> anyhow::Result<String> {
    let mut tran = get_db().begin().await?;

    let note_object = NoteObject::new(
        pref_name.to_string(), 
        content.to_string(), 
        timezone, 
        published, 
        to,
        in_reply_to, 
        attachments.clone()
    );
    let note_id = note_object.get_id();
    let note_json = note_object.to_string();
    sqlx::query!(
        r#"
insert into activities (
    object_id,
    object_json
) values ( ?, ? )
        "#,
        note_id,
        note_json)
        .execute(&mut *tran)
        .await?;

    if let Some(ref attachments) = attachments {
        let id = &note_object.get_id();
        for a in attachments {
            crate::db::insert_attachment(&mut *tran, id, a).await?;
        }
        // crate::db::insert_attachments(&note_object.get_id(), attachments).await?;
    }

    tran.commit().await?;
    log::info!("insert success. user_note_id = {}", note_id);
    Ok(note_id)
}

// search
pub async fn find_note_by_content(content: &str) -> anyhow::Result<Option<Vec<DbActivity>>> {
    let pool = get_db();

    let like = format!("%{content}%");
    let result = sqlx::query_as!(
        DbActivity,
        r#"
select * from activities
where JSON_EXTRACT(object_json, '$.content') like ?
        "#,
        like)
        .fetch_all(pool)
        .await;

    match result {
        Ok(v) if v.is_empty() => Ok(None),
        Err(sqlx::Error::RowNotFound) => Ok(None),
        Ok(v) => Ok(Some(v)),
        Err(e) => Err(e.into())
    }
}