use crate::{structs::DbFollowee, db::{get_uid, insert_external_user, is_external_user_exists}};

use super::get_db;

pub async fn get_followees(user_id: i64) -> anyhow::Result<Vec<DbFollowee>> {
    let pool = get_db();
    let record = sqlx::query_as!(
        DbFollowee,
        r#"
select * from followees
where follower_id = ?
order by followed_at
        "#,
        user_id)
        .fetch_all(pool)
        .await?;
    Ok(record)
}

pub async fn insert_followee(pref_name: String, followee_user_id: String, name: Option<String>, inbox: Option<String>, shared_inbox: Option<String>) -> anyhow::Result<()> {
    log::info!("insert_followee: pref_name={}, followee_user_id={}, follower_name={:?}, follower_inbox={:?}", pref_name, followee_user_id, name, inbox);

    let uid = match get_uid(&pref_name).await? {
        Some(i) => i, None => anyhow::bail!("user not found")
    };
    let mut tran = get_db().begin().await?;
    // checking external user is exist
    if !is_external_user_exists(&followee_user_id).await? {
        insert_external_user(&followee_user_id, name, inbox, shared_inbox, None).await;
    }

    // insert
    sqlx::query!(
        r#"
insert into followees (
    user_id, 
    follower_id
) values (
    ?,
    ?
)
        "#,
        followee_user_id,
        uid)
        .execute(&mut *tran)
        .await?;
    tran.commit().await?;

    Ok(())
}

pub async fn remove_followee(pref_name: String, followee_user_id: String) -> anyhow::Result<()> {
    log::info!("remove_followee: pref_name={}, user_id={}", pref_name, followee_user_id);

    let uid = match get_uid(&pref_name).await? {
        Some(i) => i, None => anyhow::bail!("user not found")
    };
    let mut tran = get_db().begin().await?;
    sqlx::query!(
        r#"
delete from followees
where user_id = ? and follower_id = ?
        "#,
        followee_user_id,
        uid)
        .execute(&mut *tran)
        .await?;
    tran.commit().await?;

    Ok(())
}