//! new activity management table.
//! all of activities MUST use this

use anyhow::Context;
use serde::Serialize;

use crate::structs::{DbActivity, DbActivityReceivedUser};

use super::get_db;

pub async fn get_activity_by_id(id: &str) -> anyhow::Result<Option<DbActivity>> {
    let pool = get_db();

    let r = sqlx::query_as!(
        DbActivity,
        r#"
select * from activities
where object_id = ?
        "#,
        id)
        .fetch_one(pool)
        .await;

    match r {
        Ok(a) => Ok(Some(a)),
        Err(sqlx::Error::RowNotFound) => Ok(None),
        Err(e) => Err(e.into())
    }
}

pub async fn insert_activity<A: Serialize>(a: A) -> anyhow::Result<()> {
    let mut tran = get_db().begin().await?;

    // is valid activity object?
    let activity = serde_json::to_value(a).context("not valid Json")?;
    activity.get("type").context("not valid Object")?;
    let id = activity.get("id")
        .and_then(|v| v.as_str())
        .context("invalid Activity or Activity id is not specified")?;
    let activity_string = serde_json::to_string_pretty(&activity)?;

    sqlx::query!(
        r#"
insert into activities (
    object_id, object_json
) VALUES (
    ?, ?
)
        "#,
        id,
        activity_string)
        .execute(&mut *tran)
        .await?;

    tran.commit().await?;
    Ok(())
}

pub async fn delete_activity(id: &str) -> anyhow::Result<()> {
    let mut tran = get_db().begin().await?;

    let r = sqlx::query!(
        r#"
delete from activities
where object_id = ?
        "#,
        id)
        .execute(&mut *tran)
        .await;
    
    match r {
        Ok(_) => tran.commit().await?,
        Err(sqlx::Error::RowNotFound) => {},
        Err(e) => return Err(e.into())
    }
    Ok(())
}


/*
    activities_received_users
 */

pub async fn get_activity_received_users_by_id(activity_id: String) -> anyhow::Result<Vec<DbActivityReceivedUser>> {
    let pool = get_db();

    let r = sqlx::query_as!(
        DbActivityReceivedUser,
        r#"
select * from activities_received_users
where object_id = ?
        "#,
        activity_id)
        .fetch_all(pool)
        .await;

    match r {
        Ok(v) => Ok(v),
        Err(e) => Err(e.into())
    }
}