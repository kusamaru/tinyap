mod activity;
mod attachment;
mod blob;
mod external_user;
mod feed;
mod follower;
mod followee;
mod inbox;
mod note;
mod replies;
mod user;

pub use activity::*;
pub use attachment::*;
pub use blob::*;
pub use external_user::*;
pub use feed::*;
pub use follower::*;
pub use followee::*;
pub use inbox::*;
pub use note::*;
pub use replies::*;
pub use user::*;

use chrono::{Utc, DateTime};
use once_cell::sync::OnceCell;
use sqlx::{Pool, Sqlite, sqlite::{SqlitePoolOptions, SqliteConnectOptions}, ConnectOptions};
use std::{sync::Arc, str::FromStr, time::Duration};

use crate::structs::{DbKey, DbCredential, DbSession};

const PAGE_SIZE: u32 = 30;
static DB_POOL: OnceCell<Arc<DbConnection>> = OnceCell::new();
fn get_db<'a>() -> &'a Pool<Sqlite> { DB_POOL.get().unwrap().get() }

pub struct DbConnection {
    pub con: Pool<Sqlite>,
}
impl DbConnection {
    pub fn get(&self) -> &Pool<Sqlite> {
        &self.con
    }
}

async fn connect(addr: &str) -> anyhow::Result<Pool<Sqlite>> {
    let mut options = SqliteConnectOptions::from_str(addr)?;
    options = options
        .log_statements(log::LevelFilter::Debug)
        .log_slow_statements(log::LevelFilter::Debug, Duration::from_secs(1));
    let pool = SqlitePoolOptions::new().connect_with(options).await?;
    Ok(pool)
}

pub async fn init() -> anyhow::Result<()> {
    dotenv::dotenv().ok();
    let Ok(db_url) = std::env::var("DATABASE_URL") else { anyhow::bail!("DATABASE_URL is not specified") };
    let pool = match connect(&db_url).await {
        Ok(p) => p,
        Err(_) => {
            log::error!("could not connect to database");
            anyhow::bail!("connection fail");
        }
    };
    let _ = DB_POOL.set(Arc::new(DbConnection { con: pool }));
    create_admin_if_not_exists().await?;
    Ok(())
}

pub async fn create_admin_if_not_exists() -> anyhow::Result<()> {
    let pool = get_db();
    let cnt = sqlx::query!(
        r#"
select count(*) as count from users
where preferred_username = ?
        "#,
        "admin")
        .fetch_one(pool)
        .await?;
    if cnt.count == 0 {
        log::info!("Admin user isn't exist.");
        let mut tran = pool.begin().await?;
        sqlx::query!(
            r#"
insert into users (
    id, preferred_username, name, summary
) values (
    ?, ?, ?, ?
)
            "#,
            0,
            "admin",
            "admin",
            "I will be the admin of this new server.")
            .execute(&mut *tran)
            .await?;
        tran.commit().await?;
        log::info!("Admin user is successfully inserted.");
    } else {
        log::info!("Admin user exists.");
    }
    Ok(())
}

pub async fn get_admin_keypair() -> anyhow::Result<DbKey> {
    get_or_create_keypair(0).await
}

async fn get_or_create_keypair(user_id: i64) -> anyhow::Result<DbKey> {
    let pool = get_db();
    let key = sqlx::query_as!(
        DbKey,
        r#"
select * from keys
where user_id = ?
        "#,
        user_id)
        .fetch_one(pool)
        .await;
    match key {
        Ok(k) => Ok(k),
        Err(sqlx::Error::RowNotFound) => {
            use sigh::{Key, alg::{Algorithm, RsaSha256}};
            // insert new key
            let (new_secret, new_pubkey) = RsaSha256.generate_keys()
                .and_then(|(s, p)| Ok((s.to_pem()?, p.to_pem()?)))?;
            let mut tran = get_db().begin().await?;
            sqlx::query!(
                r#"
insert into keys ( user_id, pubkey, secret ) values ( ?, ?, ? )
                "#,
                user_id,
                new_pubkey,
                new_secret,)
                .execute(&mut *tran)
                .await?;
            tran.commit().await?;
            let now = chrono::Utc::now();
            Ok(DbKey {
                user_id,
                pubkey: new_pubkey,
                secret: new_secret,
                created_at: now.naive_utc()
            })
        },
        Err(e) => Err(anyhow::anyhow!(e))
    }
}

pub async fn get_user_creds(user_id: i64) -> anyhow::Result<DbCredential> {
    let pool = get_db();
    let creds = sqlx::query_as!(
        DbCredential,
        r#"
select * from credentials
where user_id = ?
        "#,
        user_id)
        .fetch_one(pool)
        .await?;
    Ok(creds)
}

pub async fn insert_user_creds(creds: DbCredential) -> anyhow::Result<()> {
    let mut tran = get_db().begin().await?;
    sqlx::query!(
        r#"
insert into credentials ( user_id, mail, passhash )
values ( ?, ?, ? )
        "#,
        creds.user_id,
        creds.mail,
        creds.passhash)
        .execute(&mut *tran)
        .await?;
    tran.commit().await?;
    Ok(())
}

pub async fn add_session(session_id: &String, user_id: i64, expires: DateTime<Utc>, user_agent: Option<String>) -> anyhow::Result<()> {
    let mut tran = get_db().begin().await?;
    sqlx::query!(
        r#"
insert into sessions ( session_id, user_id, user_agent, expires_at )
values ( ?, ?, ?, ? )
        "#,
        session_id,
        user_id,
        user_agent,
        expires)
        .execute(&mut *tran)
        .await?;
    tran.commit().await?;
    Ok(())
}

pub async fn clear_expired_sessions() -> anyhow::Result<()> {
    let mut tran = get_db().begin().await?;
    let r = sqlx::query!(
        r#"
delete from sessions
where expires_at < current_timestamp
        "#)
        .execute(&mut *tran)
        .await?;
    tran.commit().await?;

    if r.rows_affected() > 0 {
        log::info!("clear_expired_sessions: affected {} row(s)", r.rows_affected());
    }
    Ok(())
}

pub async fn get_session(session_id: impl Into<String>) -> anyhow::Result<DbSession> {
    let pool = get_db();
    let session_id: String = session_id.into();
    let session = sqlx::query_as!(
        DbSession,
        r#"
select * from sessions
where session_id = ?
        "#,
        session_id)
        .fetch_one(pool)
        .await?;
    Ok(session)
}