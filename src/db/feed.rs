//! feed

use chrono::{DateTime, Utc};

use crate::{db::{self, PAGE_SIZE, get_db}, structs::{DbActivity, DbUser}};

pub async fn get_all_activities(page: u32) -> anyhow::Result<Vec<DbActivity>> {
    let pool = get_db();

    let page = if page == 0 { 1 } else { page };
    let offset = (page - 1) * PAGE_SIZE;

    let addr = format!("{}%", crate::to_url(String::new()));
    let r = sqlx::query_as!(
        DbActivity,
        r#"
select * from activities
where
    JSON_EXTRACT(object_json, '$.attributedTo') like ?
order by JSON_EXTRACT(object_json, '$.published') desc
limit ?, ?
        "#,
        addr,
        offset,
        PAGE_SIZE)
        .fetch_all(pool)
        .await;

    match r {
        Ok(v) => Ok(v),
        Err(sqlx::Error::RowNotFound) => Ok(vec![]),
        Err(e) => {
            log::error!("Error: {}", e.to_string());
            Err(e.into())
        }
    }
}

pub async fn is_activities_next_page_available(page: u32) -> anyhow::Result<bool> {
    let pool = get_db();

    let addr = format!("{}%", crate::to_url(String::new()));
    let r = sqlx::query!(
        r#"
select count(*) as count from activities
where JSON_EXTRACT(object_json, '$.attributedTo') like ?
        "#,
        addr)
        .fetch_one(pool)
        .await?;

    let paged_count: i64 = (page * PAGE_SIZE) as i64;
    if r.count as i64 > paged_count {
        // next page is exist
        Ok(true)
    } else {
        // not exist
        Ok(false)
    }
}

pub async fn get_posted_activities_and_received_activities(
    uid: i64,
    from: DateTime<Utc>,
    to: DateTime<Utc>,
    page: u32,
) -> anyhow::Result<Vec<DbActivity>> {
    let pool = get_db();

    let page = if page == 0 { 1 } else { page };
    let offset = (page - 1) * PAGE_SIZE;
    let DbUser {preferred_username, ..} = db::get_user_by_id(uid).await?;
    let attributed_to = crate::to_url(format!("/u/{preferred_username}"));
    
    let r = sqlx::query_as!(
        DbActivity,
        r#"
select * from activities
WHERE
    -- time constraint
    datetime(JSON_EXTRACT(object_json, '$.published')) between ? and ?
    AND
    -- user activity
    JSON_EXTRACT(object_json, '$.attributedTo') = ? 
    OR
    -- external activity received by the user
    EXISTS(
        select * from activities_received_users
        where received_user_id = ?
    )
order by JSON_EXTRACT(object_json, '$.published') desc
limit ?, ?
        "#,
        from,
        to,
        attributed_to,
        uid,
        offset,
        PAGE_SIZE)
        .fetch_all(pool)
        .await;

    match r {
        Ok(v) => Ok(v),
        Err(sqlx::Error::RowNotFound) => Ok(vec![]),
        Err(e) => {
            log::error!("Error: {}", e.to_string());
            Err(e.into())
        }
    }
}