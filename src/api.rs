use http::StatusCode;
use poem::{handler, web::Json};

use crate::{db, auth, structs::DbCredential};

#[handler]
pub async fn register(Json(req): Json<serde_json::Value>) -> poem::Result<StatusCode> {
    let mail = req.get("mail").ok_or(StatusCode::BAD_REQUEST)?
        .as_str().unwrap();
    let username = req.get("username").ok_or(StatusCode::BAD_REQUEST)?
        .as_str().unwrap();
    let password = req.get("password").ok_or(StatusCode::BAD_REQUEST)?
        .as_str().unwrap();
    db::insert_user(username.into(), username.into(), Some(9), None).await?;
    let (user, _) = db::get_user_by_pref_name(&username.to_string()).await?.unwrap();
    let hashed_password = auth::hashing_password(password.into()).unwrap();
    let creds = DbCredential {
        mail: mail.into(),
        user_id: user.id,
        passhash: hashed_password,
        updated_at: chrono::Utc::now().naive_utc(),
    };
    db::insert_user_creds(creds).await?;
    Ok(StatusCode::OK)
}