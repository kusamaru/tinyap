use std::{io::{BufReader, BufWriter}, fs::OpenOptions};

use serde::{Deserialize, Serialize};

const CONFIG_GENERAL_PATH: &str = "./config/general.json";

#[derive(Deserialize, Serialize)]
pub struct GeneralConfig {
    pub host_name: String,
    pub publish_addr: String,
    pub publish_port: u32,
    pub session_expires: u64,
}
impl Default for GeneralConfig {
    fn default() -> Self {
        Self { 
            host_name: "your.hostname.com".into(),
            publish_addr: "localhost".into(),
            publish_port: 10111,
            session_expires: 43200,
        }
    }
}
pub fn load_general_config() -> Result<GeneralConfig, std::io::Error> {
    log::info!("loading config...");
    let file = match std::fs::File::open(CONFIG_GENERAL_PATH) {
        Ok(f) => f,
        Err(e) if e.kind() == std::io::ErrorKind::NotFound => {
            log::warn!("config file 'config/general.json' does not exist!");
            log::warn!("generate default file. check and edit if necessary.");
            write_config(GeneralConfig::default(), CONFIG_GENERAL_PATH)?;
            return Err(e)
        },
        Err(e) => return Err(e)
    };
    let reader = BufReader::new(file);
    log::info!("general config is successfully loaded.");
    Ok(serde_json::from_reader(reader)?)
}

fn write_config<T: Serialize, S: AsRef<str>>(config: T, path: S) -> Result<(), std::io::Error> {
    let file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(path.as_ref())?;
    let writer = BufWriter::new(file);
    let _ = serde_json::to_writer_pretty(writer, &config);
    Ok(())
}