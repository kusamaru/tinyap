FROM clux/muslrust:1.67.1 as builder

WORKDIR /app
COPY . /app

RUN --mount=type=cache,target=~/.cargo/bin \
    cargo install sqlx-cli
RUN cargo sqlx prepare
RUN rm /app/.env

RUN --mount=type=cache,target=/usr/local/cargo/registry \
    --mount=type=cache,target=/app/target \
    cargo build -F http --release --target=x86_64-unknown-linux-musl \
    && mkdir release \
    && mv target/x86_64-unknown-linux-musl/release/tinyap /app/release

FROM alpine:latest as runner
WORKDIR /app
COPY --from=builder \
    /app/release \
    .

RUN apk update \
    && apk add --no-cache libgcc libc6-compat curl \
    && ln -s /lib/libc.musl-x86_64.so.1 /lib/ld-linux-x86-64.so.2

EXPOSE 10111
ENTRYPOINT [ "/app/tinyap" ]

# FROM rust:1.67.1-alpine
# WORKDIR /app
# COPY . /app
# RUN apk update \
#     && apk add alpine-sdk

# ENTRYPOINT [ "cargo", "run", "--release", "-F", "http" ]